//
//  KeyboardViewController.h
//  iOSKeyboardTemplate
//
//  Copyright (c) 2014 BJH Studios. All rights reserved.
//  questions or comments contact jeff@bjhstudios.com

#import <UIKit/UIKit.h>
#import <FLAnimatedImage/FLAnimatedImageView.h>
@import Firebase;
@import FirebaseDatabase;
@interface KeyboardViewController : UIInputViewController
{
    NSMutableArray *mojiArray,*myMojisArray;
    NSInteger indexPathVal;
    NSIndexPath *longPressIndex;
    NSString *firstStr,*userId;
    IBOutlet UIView *addView;
    IBOutlet UIActivityIndicatorView *indicator;
}
@property (strong, nonatomic) FIRDatabaseReference *ref;

@end
