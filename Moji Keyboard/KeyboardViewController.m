//
//  KeyboardViewController.m
//  iOSKeyboardTemplate
//
//  Copyright (c) 2014 BJH Studios. All rights reserved.
//  questions or comments contact jeff@bjhstudios.com

#import "KeyboardViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface KeyboardViewController ()<UIGestureRecognizerDelegate> {
    

    
}
@property (weak, nonatomic) IBOutlet UICollectionView *mojiCollec,*catCollec;

@property (nonatomic) CGFloat portraitHeight;
@property (nonatomic) CGFloat landscapeHeight;
@property (nonatomic) BOOL isLandscape;
@property (nonatomic) NSLayoutConstraint *heightConstraint;
@property (nonatomic) IBOutlet UIButton *nextKeyboardButton;
@property (nonatomic) IBOutlet UILabel *toastLbl;
@end

@implementation KeyboardViewController


- (void)updateViewConstraints {
    [super updateViewConstraints];
    // Add custom view sizing constraints here
    if (self.view.frame.size.width == 0 || self.view.frame.size.height == 0)
        return;
    
    [self.view.inputView removeConstraint:self.heightConstraint];
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    CGFloat screenH = screenSize.height;
    CGFloat screenW = screenSize.width;
    BOOL isLandscape =  !(self.view.frame.size.width == (screenW*(screenW<screenH))+(screenH*(screenW>screenH)));
    self.isLandscape = isLandscape;
    if (isLandscape) {
        self.heightConstraint.constant = self.landscapeHeight;
        [self.view addConstraint:self.heightConstraint];
    } else {
        self.heightConstraint.constant = self.portraitHeight;
        [self.view addConstraint:self.heightConstraint];
    }
    NSLog(@"done");
}

-(void)viewDidLayoutSubviews {

    self.landscapeHeight = 220;
    if ([[UIScreen mainScreen]bounds].size.height==736){
        _portraitHeight=300;
    } else if ([[UIScreen mainScreen]bounds].size.height==667){
        _portraitHeight=290;
    } else if ([[UIScreen mainScreen]bounds].size.width==568 || [[UIScreen mainScreen]bounds].size.width==480) {
        self.landscapeHeight = 200;
    } else {
        _portraitHeight=270;
    }

    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    CGFloat screenH = screenSize.height;
    CGFloat screenW = screenSize.width;
    BOOL isLandscape =  !(self.view.frame.size.width == (screenW*(screenW<screenH))+(screenH*(screenW>screenH)));
    if (isLandscape != _isLandscape) {
        [self updateViewConstraints];
        [_mojiCollec reloadData];
        [_catCollec reloadData];
    }

}

- (IBAction) goToApp: (id)sender {
    UIResponder* responder = self;
    while ((responder = [responder nextResponder]) != nil)
    {
        NSLog(@"responder = %@", responder);
        if([responder respondsToSelector:@selector(openURL:)] == YES)
        {
            [responder performSelector:@selector(openURL:) withObject:[NSURL URLWithString:@"mojiapp://"]];
        }
    }
    
    
}





-(void)viewDidAppear:(BOOL)animated{
    if (SYSTEM_VERSION_LESS_THAN(@"10.0")) {
        _landscapeHeight=_landscapeHeight+1;
        _portraitHeight=_portraitHeight+1;
        [self updateViewConstraints];
    }
    [super viewDidAppear:animated];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
//    if FIRApp.defaultApp() == nil {
//        FIRApp.configure()
//    }
    
    
    if ([FIRApp defaultApp]==nil) {
        [FIRApp configure];
    }
    
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.moji.Mojiapp"];
    [userDefaults synchronize];
    NSLog(@"extension %@",[userDefaults stringForKey:@"keyboard_user_id"]);
    userId=[userDefaults stringForKey:@"keyboard_user_id"];



    indexPathVal=0;
    self.landscapeHeight = 220;
    if ([[UIScreen mainScreen]bounds].size.height==736){
        _portraitHeight=300;
    } else if ([[UIScreen mainScreen]bounds].size.height==667){
        _portraitHeight=290;
    } else if ([[UIScreen mainScreen]bounds].size.width==568 || [[UIScreen mainScreen]bounds].size.width==480) {
        self.landscapeHeight = 200;
    } else {
        _portraitHeight=270;
    }

    mojiArray=[[NSMutableArray alloc]initWithObjects:@"Vector Smart Object",@"Vector Smart Object2",@"Vector Smart Object3",@"Vector Smart Object4",@"Vector Smart Object5",@"Vector Smart Object6",@"Vector Smart Object3",@"Vector Smart Object",@"Vector Smart Object4",@"Vector Smart Object2",@"Vector Smart Object6",@"Vector Smart Object5",@"Vector Smart Object",@"Vector Smart Object2",@"Vector Smart Object3",@"Vector Smart Object4",@"Vector Smart Object5",@"Vector Smart Object6",@"Vector Smart Object3",@"Vector Smart Object",@"Vector Smart Object4",@"Vector Smart Object2",@"Vector Smart Object6",@"Vector Smart Object5",@"Vector Smart Object",@"Vector Smart Object2",@"Vector Smart Object3",@"Vector Smart Object4",@"Vector Smart Object5",@"Vector Smart Object6",@"Vector Smart Object3",@"Vector Smart Object",@"Vector Smart Object4",@"Vector Smart Object2",@"Vector Smart Object6",@"Vector Smart Object5",@"Vector Smart Object",@"Vector Smart Object2",@"Vector Smart Object3",@"Vector Smart Object4",@"Vector Smart Object5",@"Vector Smart Object6",@"Vector Smart Object3",@"Vector Smart Object",@"Vector Smart Object4",@"Vector Smart Object2",@"Vector Smart Object6",@"Vector Smart Object5", nil];


    NSLayoutConstraint *nextKeyboardButtonLeftSideConstraint = [NSLayoutConstraint constraintWithItem:self.nextKeyboardButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0];
    NSLayoutConstraint *nextKeyboardButtonBottomConstraint = [NSLayoutConstraint constraintWithItem:self.nextKeyboardButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    [self.view addConstraints:@[nextKeyboardButtonLeftSideConstraint, nextKeyboardButtonBottomConstraint]];
    
    
    
    self.heightConstraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:0.0 constant:self.portraitHeight];
    
    self.heightConstraint.priority = UILayoutPriorityRequired - 1; // This will eliminate the constraint conflict warning.
    
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    [self.mojiCollec addGestureRecognizer:lpgr];
    
    [indicator startAnimating];
    [self fetchMyProjects];
}

-(void)fetchMyProjects {
    _ref = [[FIRDatabase database] reference];
    
    [[_ref child:@"keyboards"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Get user value
        if (snapshot.value != (id)[NSNull null]) {
            NSMutableArray *array=[[NSMutableArray alloc]init];
            array=snapshot.value;
            
            [array removeObjectIdenticalTo:[NSNull null]];
            
            
            
            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"moji_userid ==[c] %@",userId];
            NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"moji_status ==[c] %@",@"1"];
            
            
            NSCompoundPredicate *predicate=predicate=[NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1,predicate2]];
            array = [[array filteredArrayUsingPredicate:predicate] mutableCopy];
            
            
            
            NSMutableArray *tempArray=[[NSMutableArray alloc]init];
            tempArray=[array mutableCopy];
            for (int i=0; i<array.count; i++) {
                
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[array objectAtIndex:i];
                NSArray *mojisArray=[[array valueForKey:@"mojis"] objectAtIndex:i];
                
                if ([mojisArray isKindOfClass:[NSString class]]) {
                    [tempArray removeObject:dict];
                }
            }
            
            myMojisArray=tempArray;
            if (myMojisArray.count==0) {
                addView.hidden=NO;
            } else {
                if (indexPathVal==myMojisArray.count) {
                    indexPathVal=indexPathVal-1;
                }
                addView.hidden=YES;
            }
            
        } else {
            myMojisArray=[[NSMutableArray alloc]init];
            addView.hidden=NO;
        }
        
        [_mojiCollec reloadData];
        [_catCollec reloadData];

        [indicator stopAnimating];
        indicator.hidden=YES;
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        [indicator stopAnimating];
        indicator.hidden=YES;
    }];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    CGPoint p = [gestureRecognizer locationInView:self.mojiCollec];
    
    NSIndexPath *indexPath = [self.mojiCollec indexPathForItemAtPoint:p];
    if (indexPath == nil){
        NSLog(@"couldn't find index path");
    } else {
        NSLog(@"find index path %ld",(long)indexPath.row);

        NSArray *mojisArray=[[myMojisArray valueForKey:@"mojis"] objectAtIndex:indexPathVal];
        if ([[[mojisArray valueForKey:@"type"] objectAtIndex:indexPath.row] isEqualToString:@"gif"]) {
            longPressIndex=indexPath;
        }

        [self.mojiCollec reloadData];
    }
}

/*-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    CGPoint location = [gestureRecognizer locationInView:self.mojiCollec];
    NSIndexPath *indexPath = [self.mojiCollec indexPathForItemAtPoint:location];
    if (indexPath == nil){
        NSLog(@"couldn't find index path");
    } else {
        NSLog(@"find index path %ld",(long)indexPath.row);
        UICollectionViewCell* cell = [self.mojiCollec cellForItemAtIndexPath:indexPath];

    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
            UIView  *moveView = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y-100, 50, 50)];
            moveView.backgroundColor = [UIColor redColor];
             [self.view addSubview:moveView];
        
        [UIView animateWithDuration:0.6 animations:^{
            CGAffineTransform moveTrans = CGAffineTransformMakeTranslation(0, -40);
            moveView.transform = moveTrans;
        }];
        NSLog(@"Long press");
    } else if (gestureRecognizer.state == UIGestureRecognizerStateEnded || gestureRecognizer.state == UIGestureRecognizerStateCancelled || gestureRecognizer.state == UIGestureRecognizerStateFailed) {
        [UIView animateWithDuration:0.6 animations:^{
            CGAffineTransform moveTrans = CGAffineTransformMakeTranslation(0, 40);
         //   moveView.transform = moveTrans;
            NSLog(@"long press done");
        }];
    }
    }
}
*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
#pragma CollectioView Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView.tag==0) {
        if (myMojisArray.count==0) {
            return 0;
        }
       
        NSArray *mojisArray=[[myMojisArray valueForKey:@"mojis"] objectAtIndex:indexPathVal];
        if ([mojisArray isKindOfClass:[NSString class]]) {
            mojisArray=[[NSMutableArray alloc]init];
        }

        return mojisArray.count;
    }
    return myMojisArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag==0) {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KeyboardMojiCell" forIndexPath:indexPath];
        UIImageView *playImageView = (UIImageView *)[cell viewWithTag:101];
        UILabel *titleLbl = (UILabel *)[cell viewWithTag:102];
        FLAnimatedImageView *gifImgView = (FLAnimatedImageView *)[cell viewWithTag:100];
        UIImageView *mojiImageView = (UIImageView *)[cell viewWithTag:99];
        gifImgView.hidden=YES;
        mojiImageView.hidden=YES;
 
        UIActivityIndicatorView *indicatorr = (UIActivityIndicatorView *)[cell viewWithTag:2];
        [indicatorr startAnimating];
        indicatorr.hidden=NO;

        NSArray *mojisArray=[[myMojisArray valueForKey:@"mojis"] objectAtIndex:indexPathVal];

        titleLbl.text=[[mojisArray objectAtIndex:indexPath.row] valueForKey:@"name"];
        
        if ([[[mojisArray valueForKey:@"type"] objectAtIndex:indexPath.row] isEqualToString:@"gif"]) {
            playImageView.hidden=NO;
        } else {
            playImageView.hidden=YES;
        }

        if (indexPath.row==longPressIndex.row && (longPressIndex)) {
            
            [gifImgView sd_setImageWithURL:[NSURL URLWithString:[[mojisArray objectAtIndex:indexPath.row]valueForKey:@"image"]] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                [indicatorr stopAnimating];
                indicatorr.hidden=YES;
            }];
            gifImgView.hidden=NO;
            playImageView.hidden=YES;
        } else {
            
            [mojiImageView sd_setImageWithURL:[NSURL URLWithString:[[mojisArray objectAtIndex:indexPath.row]valueForKey:@"image"]] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                [indicatorr stopAnimating];
                indicatorr.hidden=YES;
            }];

            mojiImageView.hidden=NO;
        }
        

        
        return cell;
    } else {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KeyboardCell" forIndexPath:indexPath];
        FLAnimatedImageView *mojiImageView = (FLAnimatedImageView *)[cell viewWithTag:100];
        [mojiImageView sd_setImageWithURL:[NSURL URLWithString:[[myMojisArray objectAtIndex:indexPath.row]valueForKey:@"image"]]];
        if (indexPathVal==indexPath.row) {
            UIColor *color = [UIColor colorWithRed:192.0f/255 green:193.0f/255 blue:194.0f/255 alpha:1];
            cell.backgroundColor=color;
        } else {
            cell.backgroundColor=[UIColor clearColor];
        }
        return cell;
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView.tag==1) {
        indexPathVal=indexPath.row;
        [_catCollec reloadData];
        [_mojiCollec reloadData];
    } else {
       
        NSArray *mojisArray=[[myMojisArray valueForKey:@"mojis"] objectAtIndex:indexPathVal];
        if ([[[mojisArray valueForKey:@"type"] objectAtIndex:indexPath.row] isEqualToString:@"gif"]) {
            
            indicator.hidden=NO;
            [indicator startAnimating];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                
                
                NSString *path=[NSString stringWithFormat:@"Documents/%@.txt",[[mojisArray valueForKey:@"mojiid"] objectAtIndex:indexPath.row]];
                NSString *docPath1 = [NSHomeDirectory() stringByAppendingPathComponent:path];
                NSData *data = [[NSFileManager defaultManager] contentsAtPath:docPath1];
                
                if (!data) {
                    NSError* error = nil;
                    NSData* ImageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[mojisArray objectAtIndex:indexPath.row]valueForKey:@"image"]] options:NSDataReadingUncached error:&error];
                    if (error) {
                        
                        NSLog(@"%@", [error localizedDescription]);
                        indicator.hidden=YES;
                        [indicator stopAnimating];
                        
                        
                    } else {
                        
                        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                        [pasteboard setData:ImageData forPasteboardType:@"com.compuserve.gif"];
                        
                        NSLog(@"successfull.");
                        
                        
                        
                        NSString *path=[NSString stringWithFormat:@"Documents/%@.txt",[[mojisArray valueForKey:@"mojiid"] objectAtIndex:indexPath.row]];
                        NSString *docPath = [NSHomeDirectory() stringByAppendingPathComponent:path];
                        [ImageData writeToFile:docPath atomically:YES];

                        dispatch_sync(dispatch_get_main_queue(), ^{
                            
                            _toastLbl.hidden=NO;
                            [_toastLbl setAlpha:1];
                            [UIView animateWithDuration:3.0f animations:^{
                                [_toastLbl setAlpha:0.0f];
                            } completion:nil];
                            
                            indicator.hidden=YES;
                            [indicator stopAnimating];
                            
                        });
                    }

                } else {
                   
                    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                    [pasteboard setData:data forPasteboardType:@"com.compuserve.gif"];

                    dispatch_sync(dispatch_get_main_queue(), ^{
                        
                        _toastLbl.hidden=NO;
                        [_toastLbl setAlpha:1];
                        [UIView animateWithDuration:3.0f animations:^{
                            [_toastLbl setAlpha:0.0f];
                        } completion:nil];
                        
                        indicator.hidden=YES;
                        [indicator stopAnimating];
                        
                    });

                }

            });
            
            
        } else {
            
            UIImageView *imagee=[[UIImageView alloc]init];
            [imagee sd_setImageWithURL:[NSURL URLWithString:[[mojisArray objectAtIndex:indexPath.row]valueForKey:@"image"]] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                [UIPasteboard generalPasteboard].image = image;
                _toastLbl.hidden=NO;
                [_toastLbl setAlpha:1];
                [UIView animateWithDuration:3.0f animations:^{
                    [_toastLbl setAlpha:0.0f];
                } completion:nil];


            }];

        }

        
        
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView.tag==0) {
        if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
            return CGSizeMake(61, 64);
        } else if ([[UIScreen mainScreen]bounds].size.height==736){
            return CGSizeMake(78, 79);
        } else if ([[UIScreen mainScreen]bounds].size.width==736){
            return CGSizeMake(68, 74);
        } else if ([[UIScreen mainScreen]bounds].size.width==568 || [[UIScreen mainScreen]bounds].size.width==480) {
            return CGSizeMake(66, 69);
        } else {
            return CGSizeMake(70, 74);
        }
    } else {
        if ([[UIScreen mainScreen]bounds].size.height==736){
            return CGSizeMake(46, 46);
        }else if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
            return CGSizeMake(43 , 45);
        } else if ([[UIScreen mainScreen]bounds].size.width==568 || [[UIScreen mainScreen]bounds].size.width==480) {
            return CGSizeMake(43, 45);
        }
        return CGSizeMake(47 , 47);
    }
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView.tag==0) {
        if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
            return 4;
        } else if ([[UIScreen mainScreen]bounds].size.width==667){
            return 4;
        }
        return 6;
    }
    if ([[UIScreen mainScreen]bounds].size.height==736){
        return 9;
    } else if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
        return 3;
    } else if ([[UIScreen mainScreen]bounds].size.width==568 || [[UIScreen mainScreen]bounds].size.width==480) {
        return 4;
    }
     return 11;
}
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if ([scrollView isKindOfClass:[UICollectionView class]]) {
        UICollectionView* collectionView = (UICollectionView*)scrollView;
        
        if ([collectionView.collectionViewLayout isKindOfClass:[UICollectionViewFlowLayout class]]) {
            UICollectionViewFlowLayout* layout = (UICollectionViewFlowLayout*)collectionView.collectionViewLayout;
            
            CGFloat pageWidth = layout.itemSize.width + layout.minimumInteritemSpacing;
            
            if (collectionView.tag==0) {
                
                if ([[UIScreen mainScreen]bounds].size.height==736){
                    pageWidth = 84;
                } else if ([[UIScreen mainScreen]bounds].size.width==736){
                    pageWidth = 78;
                } else if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
                    pageWidth = 65;
                } else if ([[UIScreen mainScreen]bounds].size.width==667){
                    pageWidth = 74;
                }
            } else {
                
                if ([[UIScreen mainScreen]bounds].size.height==736){
                    pageWidth = 57;
                } else if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
                    pageWidth = 46;
                }  else if ([[UIScreen mainScreen]bounds].size.width==568 || [[UIScreen mainScreen]bounds].size.width==480) {
                    pageWidth = 50;
                } else {
                    pageWidth = 58;
                }
            }
            
            
            CGFloat usualSideOverhang = (scrollView.bounds.size.width - pageWidth)/2.0;
            // k*pageWidth - usualSideOverhang = contentOffset for page at index k if k >= 1, 0 if k = 0
            // -> (contentOffset + usualSideOverhang)/pageWidth = k at page stops
            
            NSInteger targetPage = 0;
            CGFloat currentOffsetInPages = (scrollView.contentOffset.x + usualSideOverhang)/pageWidth;
            targetPage = velocity.x < 0 ? floor(currentOffsetInPages) : ceil(currentOffsetInPages);
            targetPage = MAX(0,MIN(mojiArray.count - 1,targetPage));
            
            *targetContentOffset = CGPointMake(MAX(targetPage*pageWidth - usualSideOverhang,0), 0);
        }
    }
    
}

#pragma mark - key methods

- (IBAction) Globe:(id)sender {
    
    [self advanceToNextInputMode];
}
-(IBAction)Back:(id)sender {
    [self.textDocumentProxy deleteBackward];
}



@end
