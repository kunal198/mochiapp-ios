//
//  AppDelegate.m
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import "AppDelegate.h"
#import "Home.h"
#import "Guide.h"
#import <AdobeCreativeSDKCore/AdobeCreativeSDKCore.h>

@import Firebase;

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[AdobeUXAuthManager sharedManager] setAuthenticationParametersWithClientID:@"51e96308dafb449aa9667a0a99ab3af7" clientSecret:@"df332d35-83d3-442c-9fdd-13c10f9326dd" additionalScopeList:@[AdobeAuthManagerUserProfileScope,AdobeAuthManagerEmailScope,AdobeAuthManagerAddressScope]];
    
    // Also set the redirect URL, which is required by the CSDK authentication mechanism.
    [AdobeUXAuthManager sharedManager].redirectURL = [NSURL URLWithString:@"ams+65c755f87ff451a30fa4d39ddc69cff4c858b6c6://adobeid/51e96308dafb449aa9667a0a99ab3af7"];

    [FIRApp configure];
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    

    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UINavigationController *navObj;
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"isGuide"] ==nil) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"first" forKey:@"isGuide"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        Guide *guide=[[Guide alloc]initWithNibName:@"Guide" bundle:nil];
        navObj = [[UINavigationController alloc] initWithRootViewController:guide];
    } else {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"isGuide"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        Home *viewObj = [[Home alloc] initWithNibName:@"Home" bundle:nil];
        navObj = [[UINavigationController alloc] initWithRootViewController:viewObj];
    }
    
    self.window.rootViewController = navObj;
    [self.window makeKeyAndVisible];
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    if ([url.absoluteString isEqualToString:@"mojiapp://"]) {
        return YES;
    }
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    return handled;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
