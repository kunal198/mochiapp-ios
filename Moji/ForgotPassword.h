//
//  ForgotPassword.h
//  Moji
//
//  Created by Brst-Pc109 on 15/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"

@import Firebase;
@import FirebaseAuth;
@import FirebaseDatabase;
@interface ForgotPassword : UIViewController<BSKeyboardControlsDelegate>
{
    IBOutlet UITextField *emailTxt;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UIButton *sendBtn;
}
@end
