//
//  ForgotPassword.m
//  Moji
//
//  Created by Brst-Pc109 on 15/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import "ForgotPassword.h"

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface ForgotPassword ()
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

@end

@implementation ForgotPassword

- (void)viewDidLoad {
    [super viewDidLoad];
    indicator.hidden=YES;
    
    
    sendBtn.layer.cornerRadius=22;
    sendBtn.clipsToBounds=YES;
    [emailTxt setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];

    NSArray *fields = @[ emailTxt];
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];

}

-(void) alertView:(NSString *)title message:(NSString *)messageStr {
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:title message:messageStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)Back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)Send:(id)sender {
    if (emailTxt.text.length>0) {
        BOOL checkEmail= [self NSStringIsValidEmail:emailTxt.text];
        if (checkEmail) {
            indicator.hidden=NO;
            [indicator startAnimating];
            [[FIRAuth auth] sendPasswordResetWithEmail:emailTxt.text completion:^(NSError *error) {
                if (error) {
                    [self alertView:@"" message:error.localizedDescription];
                } else {
                    [self alertView:@"Suucessfully Sent" message:@"Check your email to reset password !"];
                }
                [indicator stopAnimating];
                indicator.hidden=YES;
            }];
        } else {
            [self alertView:@"" message:@"The email address is not valid."];
        }

    } else {
        [self alertView:@"" message:@"Enter your email !"];
    }
}


#pragma mark -
#pragma mark Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)sender {
    [self.keyboardControls setActiveField:sender];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    [emailTxt resignFirstResponder];
}

#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction {
    UIView *view;
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        view = field.superview.superview;
    } else {
        view = field.superview.superview.superview;
    }
    
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
