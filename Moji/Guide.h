//
//  Guide.h
//  PageApp
//
//  Created by Rafael Garcia Leiva on 10/06/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Guide : UIViewController <UIPageViewControllerDataSource>
{
    IBOutlet UIButton *skipBtn;
    NSMutableArray *guideImgArray,*guideTxtArray,*guideTxtImgArray;
}
@property (strong, nonatomic) UIPageViewController *pageController;

@end
