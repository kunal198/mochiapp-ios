//
//  Guide.m
//  PageApp
//
//  Created by Rafael Garcia Leiva on 10/06/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "Guide.h"
#import "GuideChild.h"
#import "Home.h"
@interface Guide ()

@end

@implementation Guide

- (void)viewDidLoad {
    
    self.navigationController.navigationBarHidden=YES;

    [super viewDidLoad];
    


    
    guideImgArray=[NSMutableArray arrayWithObjects:@"guide2",@"guide3",@"guide4",@"guide5", nil];
    guideTxtImgArray=[NSMutableArray arrayWithObjects:@"text2",@"text3",@"text4",@"text5", nil];

    
    guideTxtArray=[NSMutableArray arrayWithObjects:@"Open system settings >\nGo to General > Keyboard >\nKeyboards > Add New Keyboard",@"Add \"Moji\" from third party \nKeyboards",@"Select \"Moji\" keyboard and turn on \"Allow Full Access\"",@"Go to Messages and Switch to the Moji keyboard by tapping the globe in the keyboard. Enjoy!", nil];
    
    skipBtn.layer.borderColor=[UIColor whiteColor].CGColor;
    skipBtn.layer.borderWidth=1.5;
    skipBtn.layer.cornerRadius=22;
    skipBtn.clipsToBounds=YES;
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:[[self view] bounds]];
    if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
        [[self.pageController view] setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-105)];
    } else {
        [[self.pageController view] setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-115)];
    }
    

    
    GuideChild *initialViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    
}
-(IBAction)Skip:(id)sender {
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"isGuide"] isEqualToString: @"first"]) {
       
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"isGuide"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        Home *home=[[Home alloc]initWithNibName:@"Home" bundle:nil];
        [self.navigationController pushViewController:home animated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }

}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (GuideChild *)viewControllerAtIndex:(NSUInteger)index {
        
    GuideChild *childViewController = [[GuideChild alloc] initWithNibName:@"GuideChild" bundle:nil];
    childViewController.index = index;
    childViewController.guideImg = [guideImgArray objectAtIndex:index];
    childViewController.guideTxt=[guideTxtArray objectAtIndex:index];
    childViewController.txtImgStr=[guideTxtImgArray objectAtIndex:index];
    return childViewController;
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(GuideChild *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(GuideChild *)viewController index];
    
    index++;
    
    if (index == 4) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return 4;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}

@end
