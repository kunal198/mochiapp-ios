//
//  GuideChild.h
//  PageApp
//
//  Created by Rafael Garcia Leiva on 10/06/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuideChild : UIViewController

@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) IBOutlet UILabel *txt;
@property (strong, nonatomic) IBOutlet UIImageView *img,*textImg;
@property (strong, nonatomic) NSString *guideImg,*guideTxt,*txtImgStr;
@end
