//
//  Home.h
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>
@import FirebaseStorage;
@import FirebaseDatabase;
@interface Home : UIViewController {
    Singleton *singletonn;
}
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong, nonatomic) FIRStorageReference *storageRef;

@end
