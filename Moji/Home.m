//
//  Home.m
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import "Home.h"
#import "Login.h"
#import "Shop.h"
#import "Projects.h"
#import "Guide.h"
#import "RateApp.h"
#import "Profile.h"
#import "Upload.h"

@interface Home ()

@end

@implementation Home

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden=YES;
    singletonn=[Singleton sharedInstance];
    
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"userId"] !=nil) {
        singletonn.userId=[[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];
        singletonn.userNameStr=[[NSUserDefaults standardUserDefaults] stringForKey:@"userNameStr"];
        singletonn.userImgStr=[[NSUserDefaults standardUserDefaults] stringForKey:@"userImgStr"];
        singletonn.emailStr=[[NSUserDefaults standardUserDefaults] stringForKey:@"emailStr"];
        singletonn.isLoginstr=[[NSUserDefaults standardUserDefaults] stringForKey:@"isLoginstr"];
        singletonn.isFb=[[NSUserDefaults standardUserDefaults] stringForKey:@"isFb"];
    }
    
    [self fetchData];

    singletonn.uniqueId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
  

    
    if ([singletonn.isLoginstr isEqualToString:@"yes"]) {
        NSUserDefaults *userDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.moji.Mojiapp"];
        [userDefaults setObject:singletonn.userId forKey:@"keyboard_user_id"];
        [userDefaults synchronize];
        NSLog(@"extension %@",[userDefaults stringForKey:@"keyboard_user_id"]);

    } else {
        NSUserDefaults *userDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.moji.Mojiapp"];
        [userDefaults setObject:singletonn.uniqueId forKey:@"keyboard_user_id"];
        [userDefaults synchronize];
    }
    
}


-(void)delete {

    _ref = [[FIRDatabase database] reference];
    [[_ref child:@"reports"] observeEventType:FIRDataEventTypeChildRemoved withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.value != (id)[NSNull null]) {
            NSLog(@"%@",snapshot.value);
        }
    }withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
    }];

}

-(void)fetchData {
    _ref = [[FIRDatabase database] reference];
    [[_ref child:@"projects"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.value != (id)[NSNull null]) {
            
        }
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
    }];
    
    
    
}


- (IBAction)Projects:(id)sender {
    Projects *pro=[[Projects alloc]initWithNibName:@"Projects" bundle:nil];
    [self.navigationController pushViewController:pro animated:YES];
}
- (IBAction)Account:(id)sender {
    if ([singletonn.isLoginstr isEqualToString: @"yes"]) {
        Profile *login=[[Profile alloc]initWithNibName:@"Profile" bundle:nil];
        [self.navigationController pushViewController:login animated:YES];
        
    } else {
        Login *login=[[Login alloc]initWithNibName:@"Login" bundle:nil];
        [self.navigationController pushViewController:login animated:YES];
    }
}
- (IBAction)Shop:(id)sender {
    Shop *shop=[[Shop alloc]initWithNibName:@"Shop" bundle:nil];
    [self.navigationController pushViewController:shop animated:YES];
}
- (IBAction)Share:(id)sender {
    
    Upload *pro=[[Upload alloc]initWithNibName:@"Upload" bundle:nil];
    [self.navigationController pushViewController:pro animated:YES];
    
}
- (void)presentActivityController:(UIActivityViewController *)controller {
    
    // for iPad: make the presentation a Popover
    controller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popController.barButtonItem = self.navigationItem.leftBarButtonItem;
}

- (IBAction)Guide:(id)sender {
    Guide *guide=[[Guide alloc]initWithNibName:@"Guide" bundle:nil];
    [self.navigationController pushViewController:guide animated:YES];
}
- (IBAction)Settings:(id)sender {
    RateApp *rate=[[RateApp alloc]initWithNibName:@"RateApp" bundle:nil];
    [self.navigationController pushViewController:rate animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
