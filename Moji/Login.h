//
//  Login.h
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"
#import "Singleton.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
@import FirebaseAuth;
@import FirebaseDatabase;

@interface Login : UIViewController<BSKeyboardControlsDelegate>
{
    IBOutlet UITextField *usernameTxt,*passwordTxt;
    IBOutlet UIButton *signInBtn,*fbBtn,*registerBtn;
    BOOL keyboardIsShown;
    IBOutlet UIActivityIndicatorView *indicator;
    Singleton *singleton;
    FIRDatabaseHandle *dbHandle;
    NSMutableArray *myMojisArray,*mojiKeyArray,*myMojiFinalArray;
}

@property (strong, nonatomic) FIRDatabaseReference *dbRef;


@end
