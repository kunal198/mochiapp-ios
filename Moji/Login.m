//
//  Login.m
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import "Login.h"
#import "Register.h"
#import "Home.h"
#import "ForgotPassword.h"

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface Login ()

@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

@end

@implementation Login

- (void)viewDidLoad {
    [super viewDidLoad];
    singleton=[Singleton sharedInstance];
    if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
        signInBtn.layer.cornerRadius=20;

    } else {
        signInBtn.layer.cornerRadius=22;

    }
    
    signInBtn.clipsToBounds=YES;
    
  
    [usernameTxt setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [passwordTxt setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];

    NSArray *fields = @[ usernameTxt,passwordTxt];
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    
    indicator.hidden=YES;
    
    if (![singleton.isLoginstr isEqualToString:@"yes"]) {
        [self fetchMyProjects];
    }

 }
-(void)fetchMyProjects {
    _dbRef = [[FIRDatabase database] reference];
    [[_dbRef child:@"keyboards"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Get user value
        if (snapshot.value != (id)[NSNull null]) {
            myMojisArray=snapshot.value;
            mojiKeyArray=snapshot.value;
            myMojiFinalArray=snapshot.value;
            
            [myMojisArray removeObjectIdenticalTo:[NSNull null]];
            [myMojiFinalArray removeObjectIdenticalTo:[NSNull null]];
            
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"moji_userid ==[c] %@",singleton.uniqueId];
            myMojisArray = [[myMojisArray filteredArrayUsingPredicate:predicate] mutableCopy];
            
            
        } else {
            myMojisArray=[[NSMutableArray alloc]init];
            mojiKeyArray=[[NSMutableArray alloc]init];
            myMojiFinalArray=[[NSMutableArray alloc]init];
            
        }
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
    }];
}

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
              error:(NSError *)error {
    if (error == nil) {
        // ...
    } else {
        NSLog(@"%@", error.localizedDescription);
    }
}

- (IBAction)Back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)ForgotPassword:(id)sender {
    ForgotPassword *pass=[[ForgotPassword alloc]initWithNibName:@"ForgotPassword" bundle:nil];
    [self.navigationController pushViewController:pass animated:YES];
}

- (IBAction)SignIn:(id)sender {
    if (usernameTxt.text.length && passwordTxt.text.length>0) {
        
        
        BOOL checkEmail= [self NSStringIsValidEmail:usernameTxt.text];
        if (checkEmail) {
            indicator.hidden=NO;
            [indicator startAnimating];
            signInBtn.enabled=NO;
            fbBtn.enabled=NO;
            registerBtn.enabled=NO;
            [[FIRAuth auth] signInWithEmail:usernameTxt.text
                                   password:passwordTxt.text
                                 completion:^(FIRUser *user, NSError *error) {
                                     if (error) {
                                         [self alertView:@"" message:error.localizedDescription];
                                     } else {
                                         
                                         singleton.userId=user.uid;

                                         
                                         NSString *userID = [FIRAuth auth].currentUser.uid;
                                         _dbRef = [[FIRDatabase database] reference];
                                         [[[_dbRef child:@"users"] child:userID] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                                             // Get user value
                                             
                                             if (snapshot.value != (id)[NSNull null]) {
                                                 singleton.userNameStr=snapshot.value[@"Name"];
                                                 singleton.userImgStr=snapshot.value[@"Profile Picture"];
                                             }
                                             
                                             singleton.emailStr=usernameTxt.text;

                                             
                                             [[NSUserDefaults standardUserDefaults] setObject:singleton.userNameStr forKey:@"userNameStr"];
                                             [[NSUserDefaults standardUserDefaults] setObject:singleton.userImgStr forKey:@"userImgStr"];
                                             [[NSUserDefaults standardUserDefaults] setObject:singleton.emailStr forKey:@"emailStr"];
                                             [[NSUserDefaults standardUserDefaults] setObject:singleton.userId forKey:@"userId"];
                                             [[NSUserDefaults standardUserDefaults] synchronize];

                                             [[NSNotificationCenter defaultCenter] postNotificationName:@"getNotification" object:self];

                                             
                                         } withCancelBlock:^(NSError * _Nonnull error) {
                                             NSLog(@"%@", error.localizedDescription);
                                         }];

                                         
                                         
                                         singleton.isLoginstr=@"yes";
                                         singleton.isFb=@"no";
                                         
                                         [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"isLoginstr"];
                                         [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"isFb"];
                                         [[NSUserDefaults standardUserDefaults] synchronize];

                                         
                                         if (myMojisArray.count>0) {
                                             UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Previously, you are added some mojis in your keyboard. Do you want to sync that in your account ?" preferredStyle:UIAlertControllerStyleAlert];
                                             UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

                                                 NSLog( @"yes");
                                                 [myMojiFinalArray removeObjectsInArray:myMojisArray];

                                                 for (int i=0; i<myMojisArray.count; i++) {
                                                     NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                                                     dict=[myMojisArray objectAtIndex:i];
                                                     [dict setValue:userID forKey:@"moji_userid"];

                                                     NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"projectid ==[c] %@",[dict valueForKey:@"projectid"]];
                                                     NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"moji_userid ==[c] %@",userID];
                                                     
                                                     
                                                     NSCompoundPredicate *predicate=predicate=[NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1,predicate2]];
                                                     NSMutableArray *array = [[myMojiFinalArray filteredArrayUsingPredicate:predicate] mutableCopy];
                                                     if (array.count==0) {
                                                         [myMojiFinalArray addObject:dict];
                                                      }
                                                         

                                                     
                                                     
                                                 }
                                                 
                                                 
                                                 [[_dbRef child:@"keyboards"] setValue:myMojiFinalArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                                                     if (error) {
                                                         NSLog(@"%@",error.localizedDescription);
                                                     }
                                                 }];
                                                 
                                                 
                                                 Home *home=[[Home alloc]initWithNibName:@"Home" bundle:nil];
                                                 [self.navigationController pushViewController:home animated:YES];

                                                 
                                             }];
                                             
                                             UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                 
                                                 NSLog( @"cancel");
                                                 _dbRef = [[FIRDatabase database] reference];
                                                 [myMojiFinalArray removeObjectsInArray:myMojisArray];
                                                 [[_dbRef child:@"keyboards"]  setValue:myMojiFinalArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                                                     if (error) {
                                                         NSLog(@"%@",error.localizedDescription);
                                                     }
                                                 }];
                                                 
                                                 Home *home=[[Home alloc]initWithNibName:@"Home" bundle:nil];
                                                 [self.navigationController pushViewController:home animated:YES];
                                             }];

                                             [alertController addAction:cancel];
                                             [alertController addAction:ok];
                                             [self presentViewController:alertController animated:YES completion:nil];

                                         } else {
                                               Home *home=[[Home alloc]initWithNibName:@"Home" bundle:nil];
                                               [self.navigationController pushViewController:home animated:YES];
                                         }
                                         

                                         
                                     }
                                     NSLog(@"%@",error.localizedDescription);
                                     NSLog(@"%@",user.email);
                                     indicator.hidden=YES;
                                     [indicator stopAnimating];
                                   
                                     signInBtn.enabled=YES;
                                     fbBtn.enabled=YES;
                                     registerBtn.enabled=YES;

                                 }];
        } else {
            [self alertView:@"" message:@"The email address is not valid."];
        }

    } else {
        [self alertView:@"" message:@"Enter all fields !"];
    }
}

-(void) alertView:(NSString *)title message:(NSString *)messageStr {
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:title message:messageStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)Facebook:(id)sender {
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in");
             NSLog(@"token is %@",[FBSDKAccessToken currentAccessToken].tokenString);

             NSString *profilePic = [NSString stringWithFormat:@"http://graph.facebook.com/\%@/picture?type=large",[FBSDKAccessToken currentAccessToken].userID];
             NSLog(@"%@",profilePic);

             
             indicator.hidden=NO;
             [indicator startAnimating];
             FIRAuthCredential *credential = [FIRFacebookAuthProvider credentialWithAccessToken:[FBSDKAccessToken currentAccessToken].tokenString];
             
             signInBtn.enabled=NO;
             fbBtn.enabled=NO;
             registerBtn.enabled=NO;

             [[FIRAuth auth] signInWithCredential:credential completion:^(FIRUser *user, NSError *error) {
                 
                 if (error) {
                     [self alertView:@"" message:error.localizedDescription];
                     indicator.hidden=YES;
                     [indicator stopAnimating];

                     return;
                 } else {
                     singleton.isLoginstr=@"yes";
                     singleton.isFb=@"yes";

                     [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"isLoginstr"];
                     [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"isFb"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     
                     
                     
                     singleton.userNameStr=user.displayName;
                     singleton.userImgStr=profilePic;
                     singleton.emailStr=user.email;
                     singleton.userId=user.uid;

                     [[NSUserDefaults standardUserDefaults] setObject:singleton.userNameStr forKey:@"userNameStr"];
                     [[NSUserDefaults standardUserDefaults] setObject:singleton.userImgStr forKey:@"userImgStr"];
                     [[NSUserDefaults standardUserDefaults] setObject:singleton.emailStr forKey:@"emailStr"];
                     [[NSUserDefaults standardUserDefaults] setObject:singleton.userId forKey:@"userId"];
                     [[NSUserDefaults standardUserDefaults] synchronize];


                     
                     if (myMojisArray.count>0) {
                         UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Previously, you are added some mojis in your keyboard. Do you want to sync that in your account ?" preferredStyle:UIAlertControllerStyleAlert];
                         UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                             
                             NSLog( @"yes");
                             [myMojiFinalArray removeObjectsInArray:myMojisArray];
                             
                             for (int i=0; i<myMojisArray.count; i++) {
                                 NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                                 dict=[myMojisArray objectAtIndex:i];
                                 [dict setValue:user.uid forKey:@"moji_userid"];
                                 
                                 NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"projectid ==[c] %@",[dict valueForKey:@"projectid"]];
                                 NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"moji_userid ==[c] %@",user.uid];
                                 
                                 
                                 NSCompoundPredicate *predicate=predicate=[NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1,predicate2]];
                                 NSMutableArray *array = [[myMojiFinalArray filteredArrayUsingPredicate:predicate] mutableCopy];
                                 if (array.count==0) {
                                     [myMojiFinalArray addObject:dict];
                                 }
                                 
                                 
                                 
                                 
                             }
                             
                             
                             [[_dbRef child:@"keyboards"] setValue:myMojiFinalArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                                 if (error) {
                                     NSLog(@"%@",error.localizedDescription);
                                 }
                             }];
                             
                             
                             Home *home=[[Home alloc]initWithNibName:@"Home" bundle:nil];
                             [self.navigationController pushViewController:home animated:YES];
                             
                             
                         }];
                         
                         UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                             
                             NSLog( @"cancel");
                             _dbRef = [[FIRDatabase database] reference];
                             [myMojiFinalArray removeObjectsInArray:myMojisArray];
                             [[_dbRef child:@"keyboards"]  setValue:myMojiFinalArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                                 if (error) {
                                     NSLog(@"%@",error.localizedDescription);
                                 }
                             }];
                             
                             Home *home=[[Home alloc]initWithNibName:@"Home" bundle:nil];
                             [self.navigationController pushViewController:home animated:YES];
                         }];
                         
                         [alertController addAction:cancel];
                         [alertController addAction:ok];
                         [self presentViewController:alertController animated:YES completion:nil];
                         
                     } else {
                         Home *home=[[Home alloc]initWithNibName:@"Home" bundle:nil];
                         [self.navigationController pushViewController:home animated:YES];
                     }

                     
                     
                 }
                 indicator.hidden=YES;
                 [indicator stopAnimating];

                 signInBtn.enabled=YES;
                 fbBtn.enabled=YES;
                 registerBtn.enabled=YES;

             }];

             
         }
     }];
}
- (IBAction)Register:(id)sender {
    Register *reg=[[Register alloc]initWithNibName:@"Register" bundle:nil];
    [self.navigationController pushViewController:reg animated:YES];
}

#pragma mark -
#pragma mark Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)sender {
    
    [self.keyboardControls setActiveField:sender];
    
    if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480 || [[UIScreen mainScreen]bounds].size.height==667) {
        [self setViewMovedUp12:YES textfieldd:sender];
    }else if ([sender isEqual:passwordTxt]){
        [self setViewMovedUp12:YES textfieldd:sender];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self setViewMovedUp12:NO textfieldd:textField];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    [usernameTxt resignFirstResponder];
    [passwordTxt resignFirstResponder];
}

#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction {
    UIView *view;
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        view = field.superview.superview;
    } else {
        view = field.superview.superview.superview;
    }
    
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

-(void)setViewMovedUp12:(BOOL)movedUp textfieldd:(UITextField *)textfield {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    CGRect rect = self.view.frame;
    if (movedUp) {
        if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
            if ([textfield isEqual:usernameTxt]){
                rect.origin.y=-60;
            }else if (textfield==passwordTxt) {
                rect.origin.y=-75;
            }
        }else if ([[UIScreen mainScreen]bounds].size.height==667){
            if (textfield==usernameTxt) {
                rect.origin.y=-17;
            } else if (textfield==passwordTxt) {
                rect.origin.y=-40;
            }
        } else if (textfield==passwordTxt) {
            rect.origin.y=-28;
        }
    }
     else {
        rect.origin.y=0;
    }
    
    self.view.frame = CGRectMake(0, rect.origin.y, rect.size.width, rect.size.height);
    
    [UIView commitAnimations];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
