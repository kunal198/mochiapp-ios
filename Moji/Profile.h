//
//  Profile.h
//  Moji
//
//  Created by Brst-Pc109 on 15/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
@import FirebaseAuth;
@interface Profile : UIViewController {
    Singleton *singletonn;
    IBOutlet UITableView *profileTbl;
    IBOutlet UIView *headerView;
    NSMutableArray *projectArray,*mojiArray;
    IBOutlet UIView *countvView;
    IBOutlet UILabel *nameLbl,*emailTxt;
    IBOutlet UIImageView *userImg,*loginImg;
}
@end
