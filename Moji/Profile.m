//
//  Profile.m
//  Moji
//
//  Created by Brst-Pc109 on 15/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import "Profile.h"
#import "ProfileCell.h"
#import "ShopMojiCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Home.h"
@interface Profile ()<UICollectionViewDelegate,UICollectionViewDataSource>

@end

@implementation Profile

- (void)viewDidLoad {
    [super viewDidLoad];
   
    singletonn=[Singleton sharedInstance];
 
    
    nameLbl.text=singletonn.userNameStr;
    emailTxt.text=singletonn.emailStr;
    
    
    [userImg sd_setImageWithURL:[NSURL URLWithString:singletonn.userImgStr] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        CGFloat boundsScale=userImg.frame.size.width / userImg.frame.size.height;
        CGFloat imageScale=image.size.width / image.size.height;
        CGRect drawingRect = userImg.bounds;
        if (boundsScale > imageScale) {
            drawingRect.size.width =  drawingRect.size.height * imageScale;
            drawingRect.origin.x = (userImg.frame.size.width - drawingRect.size.width) / 2;
        } else{
            drawingRect.size.height = drawingRect.size.width / imageScale;
            drawingRect.origin.y = (userImg.frame.size.height - drawingRect.size.height) / 2;
        }
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:drawingRect cornerRadius:10];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.path = maskPath.CGPath;
        userImg.layer.mask = maskLayer;
     }];
    
    
    
    
    
    countvView.layer.cornerRadius=12;
    countvView.clipsToBounds=YES;
    
    if ([singletonn.isFb isEqualToString:@"yes"]) {
        loginImg.image=[UIImage imageNamed:@"fb3.png"];
    } else {
        loginImg.image=[UIImage imageNamed:@"email1.png"];
    }
    
    mojiArray=[[NSMutableArray alloc]initWithObjects:@"Vector Smart Object",@"Vector Smart Object2",@"Vector Smart Object3",@"Vector Smart Object4",@"Vector Smart Object5",@"Vector Smart Object6",@"Vector Smart Object3",@"Vector Smart Object",@"Vector Smart Object4",@"Vector Smart Object2",@"Vector Smart Object6",@"Vector Smart Object5",@"Vector Smart Object",@"Vector Smart Object2",@"Vector Smart Object3",@"Vector Smart Object4",@"Vector Smart Object5",@"Vector Smart Object6",@"Vector Smart Object3",@"Vector Smart Object",@"Vector Smart Object4",@"Vector Smart Object2",@"Vector Smart Object6",@"Vector Smart Object5", nil];
    
    projectArray=[[NSMutableArray alloc]initWithObjects:@"Mojis",@"Love",@"Valentine",@"Birthday",@"Games",@"Happy",@"Sad", nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveTestNotification:) name:@"getNotification" object:nil];
}

- (void) receiveTestNotification:(NSNotification *) notification {
    NSLog(@"Recieve");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"getNotification" object:nil];
    nameLbl.text=singletonn.userNameStr;
    emailTxt.text=singletonn.emailStr;
    
    
    
    [userImg sd_setImageWithURL:[NSURL URLWithString:singletonn.userImgStr] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        CGFloat boundsScale=userImg.frame.size.width / userImg.frame.size.height;
        CGFloat imageScale=image.size.width / image.size.height;
        CGRect drawingRect = userImg.bounds;
        if (boundsScale > imageScale) {
            drawingRect.size.width =  drawingRect.size.height * imageScale;
            drawingRect.origin.x = (userImg.frame.size.width - drawingRect.size.width) / 2;
        } else{
            drawingRect.size.height = drawingRect.size.width / imageScale;
            drawingRect.origin.y = (userImg.frame.size.height - drawingRect.size.height) / 2;
        }
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:drawingRect cornerRadius:10];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.path = maskPath.CGPath;
        userImg.layer.mask = maskLayer;
    }];
    
}


-(IBAction)Logout:(id)sender {

    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to logout ?" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *logout = [UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {

        NSError *signOutError;
        BOOL status = [[FIRAuth auth] signOut:&signOutError];
        if (!status) {
            NSLog(@"Error signing out: %@", signOutError);
            return;
        }
        
        
        if ([singletonn.isFb isEqualToString:@"yes"]) {
            FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
            [loginManager logOut];
            [FBSDKAccessToken setCurrentAccessToken:nil];

        }
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"userId"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"userNameStr"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"userImgStr"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"emailStr"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"isLoginstr"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"isFb"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        singletonn.userId=@"";
        singletonn.userNameStr=@"";
        singletonn.userImgStr=@"";
        singletonn.emailStr=@"";
        singletonn.isLoginstr=@"";
        singletonn.isFb=@"";

        

        Home *home=[[Home alloc]initWithNibName:@"Home" bundle:nil];
        [self.navigationController pushViewController:home animated:YES];
        
    }];
    
    [alertController addAction:logout];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];

    
}
-(IBAction)Back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return projectArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProfileCell *cell = (ProfileCell *)[tableView dequeueReusableCellWithIdentifier:@"ProfileCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProfileCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.nameLbl.text=[projectArray objectAtIndex:indexPath.row];
    cell.mojiCollec.delegate=self;
    cell.mojiCollec.dataSource=self;
    [cell.mojiCollec registerNib:[UINib nibWithNibName:@"ShopMojiCell" bundle:nil] forCellWithReuseIdentifier:@"ShopMojiCell"];
    return cell;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return headerView.frame.size.height;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return mojiArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ShopMojiCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ShopMojiCell" forIndexPath:indexPath];
    cell.mojiImg.image=[UIImage imageNamed:[mojiArray objectAtIndex:indexPath.row]];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([[UIScreen mainScreen]bounds].size.height==736 || [[UIScreen mainScreen]bounds].size.height==667) {
        return CGSizeMake(62, 62);
    } else {
        return CGSizeMake(50, 50);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
