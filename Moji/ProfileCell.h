//
//  ShopCell.h
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCell : UITableViewCell
@property(nonatomic,strong)IBOutlet UICollectionView *mojiCollec;
@property(nonatomic,strong)IBOutlet UILabel *nameLbl;
@end
