//
//  ProjectCell.h
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FLAnimatedImage/FLAnimatedImageView.h>
@interface ProjectCell : UICollectionViewCell
@property(nonatomic,strong)IBOutlet UIView *backView,*selectedView;
@property(nonatomic,strong)IBOutlet UIImageView *addImg;
@property(nonatomic,strong)IBOutlet FLAnimatedImageView *mojiImg;
@property(nonatomic,strong)IBOutlet UILabel *titleLbl;
@property(nonatomic,strong)IBOutlet UIActivityIndicatorView *indicator;
@end
