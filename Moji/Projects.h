//
//  Projects.h
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"
#import "Singleton.h"
@import FirebaseStorage;
@import FirebaseDatabase;

@interface Projects : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,BSKeyboardControlsDelegate>
{
    Singleton *singletonn;
    IBOutlet UICollectionView *projectCollec;
    NSMutableArray *projectArray,*finalArray,*keyArray;
    NSMutableArray *indexPaths;
    NSString *deleteStr;
    IBOutlet UIView *deleteView;
    NSString *titleStr;
    UIAlertAction* okAction;
    
    IBOutlet UIView *popUpView,*popUpBackView;
    IBOutlet UITextField *descriptionView;
    IBOutlet UIButton *UploadBtn,*deleteBtn;
    UIImage *projectImg;
    IBOutlet UIActivityIndicatorView *indicator;
    NSMutableArray *keyboardDeleteArray,*keyboardArray;
    NSString *isCamera;
}

@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong, nonatomic) FIRStorageReference *storageRef;

@end
