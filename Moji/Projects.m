//
//  Projects.m
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import "Projects.h"
#import "ProjectCell.h"
#import "SubCategories.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Login.h"
#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface Projects ()
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

@end

@implementation Projects

- (void)viewDidLoad {
    [super viewDidLoad];
    singletonn=[Singleton sharedInstance];
    
    [projectCollec registerNib:[UINib nibWithNibName:@"ProjectCell" bundle:nil] forCellWithReuseIdentifier:@"ProjectCell"];

    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, descriptionView.frame.size.height)];
    descriptionView.leftView = paddingView;
    descriptionView.leftViewMode = UITextFieldViewModeAlways;
    
    popUpView.layer.cornerRadius=10;
    popUpView.clipsToBounds=YES;
    
    UploadBtn.layer.cornerRadius=18;
    UploadBtn.clipsToBounds=YES;
    popUpBackView.hidden=YES;
    
    
    UIColor *color = [UIColor lightGrayColor];
    color = [color colorWithAlphaComponent:0.4f];
    
    descriptionView.layer.cornerRadius=6;
    descriptionView.layer.borderWidth=1.4;
    descriptionView.layer.borderColor=color.CGColor;
    descriptionView.clipsToBounds=YES;
    
    NSArray *fields = @[ descriptionView];
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];

    [indicator startAnimating];
    [self fetchProjects];
    
    [self fetchMyProjects];
}
-(void)fetchMyProjects {
    _ref = [[FIRDatabase database] reference];
    [[_ref child:@"keyboards"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Get user value
        if (snapshot.value != (id)[NSNull null]) {
            keyboardArray=snapshot.value;
            [keyboardArray removeObjectIdenticalTo:[NSNull null]];
        } else {
            keyboardArray=[[NSMutableArray alloc]init];
        }
    } withCancelBlock:^(NSError * _Nonnull error) {
    }];
}

-(void)fetchProjects {
    _ref = [[FIRDatabase database] reference];
    [[_ref child:@"projects"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Get user value
        
        if (snapshot.value != (id)[NSNull null]) {
          
            keyArray=snapshot.value;
            finalArray=snapshot.value;
            [finalArray removeObjectIdenticalTo:[NSNull null]];

            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userid contains[cd] %@",singletonn.userId];
            projectArray = [[finalArray filteredArrayUsingPredicate:predicate] mutableCopy];
            

        } else {
            projectArray=[[NSMutableArray alloc]init];
            finalArray=[[NSMutableArray alloc]init];
            keyArray=[[NSMutableArray alloc]init];
        }
        [projectCollec reloadData];
        indicator.hidden=YES;
        [indicator stopAnimating];

        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        indicator.hidden=YES;
        [indicator stopAnimating];

    }];
    
    
}

- (IBAction)Back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return projectArray.count+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ProjectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProjectCell" forIndexPath:indexPath];
    cell.addImg.hidden=YES;
    NSMutableDictionary *dict;
    if (indexPath.row==projectArray.count) {
        cell.mojiImg.image=[UIImage imageNamed:@"addimg"];
        cell.addImg.hidden=NO;
        cell.titleLbl.text=@"Add";
        cell.indicator.hidden=YES;
        [cell.indicator stopAnimating];
    } else {
        dict=[projectArray objectAtIndex:indexPath.row];
        cell.indicator.hidden=NO;
        [cell.indicator startAnimating];
        cell.titleLbl.text=[NSString stringWithFormat:@"%@",[[projectArray objectAtIndex:indexPath.row]valueForKey:@"name"]];
        
        
        [cell.mojiImg sd_setImageWithURL:[NSURL URLWithString:[[projectArray objectAtIndex:indexPath.row]valueForKey:@"image"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            CGFloat boundsScale=cell.mojiImg.frame.size.width / cell.mojiImg.frame.size.height;
            CGFloat imageScale=image.size.width / image.size.height;
            CGRect drawingRect = cell.mojiImg.bounds;
            if (boundsScale > imageScale) {
                drawingRect.size.width =  drawingRect.size.height * imageScale;
                drawingRect.origin.x = (cell.mojiImg.frame.size.width - drawingRect.size.width) / 2;
            } else{
                drawingRect.size.height = drawingRect.size.width / imageScale;
                drawingRect.origin.y = (cell.mojiImg.frame.size.height - drawingRect.size.height) / 2;
            }
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:drawingRect cornerRadius:2];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.path = maskPath.CGPath;
            cell.mojiImg.layer.mask = maskLayer;
            
            cell.indicator.hidden=YES;
            [cell.indicator stopAnimating];

        }];

        
    }

    cell.backView.layer.cornerRadius=14;
    cell.backView.clipsToBounds=YES;
    
    cell.selectedView.layer.cornerRadius=8;
    cell.selectedView.clipsToBounds=YES;
    

    if ([indexPaths containsObject:dict]) {
        cell.selectedView.hidden=NO;
    } else {
        cell.selectedView.hidden=YES;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if ([singletonn.isLoginstr isEqualToString:@"yes"]) {
        NSMutableArray *indexPathArray = [NSMutableArray arrayWithObject:indexPath];
        
        if (indexPath.row !=projectArray.count) {
            if ([deleteStr isEqualToString:@"select"]) {
                NSMutableDictionary *dict=[projectArray objectAtIndex:indexPath.row];
                if (indexPaths.count==0) {
                    
                    indexPaths = [NSMutableArray arrayWithObject:dict];
                    
                } else {
                    if ([indexPaths containsObject:dict]) {
                        [indexPaths removeObject:dict];
                    } else {
                        [indexPaths addObject:dict];
                    }
                }
                [collectionView reloadItemsAtIndexPaths:indexPathArray];
            } else {
                
                NSDictionary *dict=[projectArray objectAtIndex:indexPath.row];
                NSInteger anIndex=[keyArray indexOfObject:dict];
                if(NSNotFound == anIndex) {
                    NSLog(@"not found");
                }
                
                
                SubCategories *sub=[[SubCategories alloc]initWithNibName:@"SubCategories" bundle:nil];
                sub.projectArray=[[projectArray valueForKey:@"mojis"] objectAtIndex:indexPath.row];
                sub.titleStr=[NSString stringWithFormat:@"%@",[[projectArray valueForKey:@"name"] objectAtIndex:indexPath.row]];
                sub.projectIdStr=[NSString stringWithFormat:@"%@",[[projectArray valueForKey:@"projectid"] objectAtIndex:indexPath.row]];
                sub.keyStr=[NSString stringWithFormat:@"%ld",(long)anIndex];
                [self.navigationController pushViewController:sub animated:YES];
            }
        } else {
            
            if (![deleteStr isEqualToString:@"select"]) {
                [self pickImageMethod];
            }
        }
 
        
    } else {

        UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Please login to save the projects. Do you want to login?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            Login *sub=[[Login alloc]initWithNibName:@"Login" bundle:nil];
            [self.navigationController pushViewController:sub animated:YES];
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        
        [alertController addAction:cancel];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)titleMethod{
    
    
    UIAlertController *alerController = [UIAlertController alertControllerWithTitle:@"" message:@"Enter the title" preferredStyle:UIAlertControllerStyleAlert];
    [alerController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"";
        [textField addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    }];
    
    
    okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                             NSLog(@"Current password %@", [[alerController textFields][0] text]);
                             titleStr=[[alerController textFields][0] text];
        [self pickImageMethod];
                         }];
    
    
    [okAction setEnabled:false];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"Canelled");
    }];
    
    [alerController addAction:okAction];
    [alerController addAction:cancelAction];
    [self presentViewController:alerController animated:YES completion:nil];
    
    
}
-(void)textDidChange:(UITextField *)textField {
    if (textField.text.length > 0) {
        //enablet the buttons
        [okAction setEnabled:YES];
    }
}

-(IBAction)PopupCross:(id)sender {
    popUpBackView.hidden=YES;
    [descriptionView resignFirstResponder];
}


-(void)pickImageMethod {
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        isCamera=@"yes";
        
    }];
    
    UIAlertAction *gallery = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        isCamera=@"no";

    }];

    [alertController addAction:gallery];
    [alertController addAction:camera];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    

    projectImg=info[UIImagePickerControllerOriginalImage];

    CGRect crop = [[info valueForKey:@"UIImagePickerControllerCropRect"] CGRectValue];
    projectImg = [self ordinaryCrop:projectImg toRect:crop];
    
    CGRect rect = CGRectMake(0,0,150,150);
    UIGraphicsBeginImageContext( rect.size );
    [projectImg drawInRect:rect];
    UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData *imageData = UIImagePNGRepresentation(picture1);
    projectImg=[UIImage imageWithData:imageData];
    
    

    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView animateWithDuration:0.3/1.5 animations:^{
        popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                popUpView.transform = CGAffineTransformIdentity;
              //  [descriptionView becomeFirstResponder];

            }];
        }];
    }];
    
    descriptionView.text=@"";
    popUpBackView.hidden=NO;

}
-(UIImage *)ordinaryCrop:(UIImage *)imageToCrop toRect:(CGRect)cropRect
{
    CGAffineTransform rectTransform;
    switch (imageToCrop.imageOrientation)
    {
        case UIImageOrientationLeft:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(M_PI_2), 0, -imageToCrop.size.height);
            break;
        case UIImageOrientationRight:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(-M_PI_2), -imageToCrop.size.width, 0);
            break;
        case UIImageOrientationDown:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(-M_PI), -imageToCrop.size.width, -imageToCrop.size.height);
            break;
        default:
            rectTransform = CGAffineTransformIdentity;
    };
    rectTransform = CGAffineTransformScale(rectTransform, imageToCrop.scale, imageToCrop.scale);

    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], CGRectApplyAffineTransform(cropRect, rectTransform));

    UIImage *cropped = [UIImage imageWithCGImage:imageRef scale:0.1 orientation:imageToCrop.imageOrientation];
    CGImageRelease(imageRef);
    return cropped;
}

-(IBAction)Upload:(id)sender {
    if (descriptionView.text.length>0) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name ==[c] %@",descriptionView.text];
        NSMutableArray *filteredArray = [[projectArray filteredArrayUsingPredicate:predicate] mutableCopy];
        if (filteredArray.count>0) {
            
            UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"The name you entered is already exit in your projects. Do you want to upload mojis to that project?" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                
                NSDictionary *dict=[filteredArray objectAtIndex:0];
                NSInteger anIndex=[keyArray indexOfObject:dict];
                if(NSNotFound == anIndex) {
                    NSLog(@"not found");
                }

                
                
                SubCategories *sub=[[SubCategories alloc]initWithNibName:@"SubCategories" bundle:nil];
                sub.projectArray=[[filteredArray valueForKey:@"mojis"] objectAtIndex:0];
                sub.titleStr=[NSString stringWithFormat:@"%@",[[filteredArray valueForKey:@"name"] objectAtIndex:0]];
                sub.projectIdStr=[NSString stringWithFormat:@"%@",[[filteredArray valueForKey:@"projectid"] objectAtIndex:0]];
                sub.keyStr=[NSString stringWithFormat:@"%ld",(long)anIndex];
                [self.navigationController pushViewController:sub animated:YES];
                popUpBackView.hidden=YES;
                
            }];
           
            UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
            
            [alertController addAction:cancel];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];

            
            
        } else {
            
            [self uploadMethod];
        }
        [descriptionView resignFirstResponder];
    } else {
        UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Enter the title !" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];

    }
}



-(void)uploadMethod {
    
    
    
    
    [indicator startAnimating];
    indicator.hidden=NO;
    
    _ref = [[FIRDatabase database] reference];
    FIRDatabaseReference * autoId = [_ref childByAutoId];
    NSString *uniqueId =autoId.key;

    
    
    if (projectImg != nil) {
        FIRStorage *storage = [FIRStorage storage];
        _storageRef = [storage referenceForURL:@"gs://api-project-405149633111.appspot.com"];
        NSString *imageID = [[NSUUID UUID] UUIDString];
        NSString *imageName = [NSString stringWithFormat:@"ProjectPictures/%@.jpg",imageID];
        FIRStorageReference *profilePicRef = [_storageRef child:imageName];
        FIRStorageMetadata *metadata = [[FIRStorageMetadata alloc] init];
          metadata.contentType = @"image/jpeg";
        
        NSData *imageData ;
        
        
        if ([isCamera isEqualToString:@"yes"]) {
            imageData = UIImageJPEGRepresentation(projectImg, 0.0);
        } else {
        imageData = UIImagePNGRepresentation(projectImg);
        }
      
        deleteBtn.enabled=NO;
        UploadBtn.enabled=NO;

        [profilePicRef putData:imageData metadata:metadata completion:^(FIRStorageMetadata *metadata, NSError *error) {
            if (!error) {
                NSString *profileImageURL = metadata.downloadURL.absoluteString;
                
                
                NSMutableArray *array=[[NSMutableArray alloc]initWithObjects:@{@"name": descriptionView.text, @"image":profileImageURL,@"userid": singletonn.userId, @"status":@"0",@"description": @"", @"projectid":uniqueId,@"download": @"0",@"mojis":@"",@"flag":@"0",@"username":singletonn.userNameStr,@"userimage":singletonn.userImgStr},nil];
                [projectArray addObjectsFromArray:array];
                [finalArray addObjectsFromArray:array];
                
                [[_ref child:@"projects"] setValue:finalArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                    if (error) {
                        NSLog(@"%@",error.localizedDescription);
                        [self alertView:@"" message:error.localizedDescription];
                    } else {
                        popUpBackView.hidden=YES;
                        [projectCollec reloadData];
                      //  [self alertView:@"" message:@"Project has been successfully uploaded !"];

                    }
                    [indicator stopAnimating];
                    indicator.hidden=YES;

                    deleteBtn.enabled=YES;
                    UploadBtn.enabled=YES;

                }];
            }
            else if (error) {
                [self alertView:@"" message:error.localizedDescription];

                NSLog(@"Failed to Register User with profile image");
                [indicator stopAnimating];
                indicator.hidden=YES;
                
                deleteBtn.enabled=YES;
                UploadBtn.enabled=YES;

            }
        }];
    }
}

-(void) alertView:(NSString *)title message:(NSString *)messageStr {
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:title message:messageStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
        return CGSizeMake(100, 114);
    } else if ([[UIScreen mainScreen]bounds].size.height==736){
        return CGSizeMake(98, 112);
    } else {
        return CGSizeMake(112, 124);
    }
}

- (IBAction)Share:(id)sender {
    
    NSString *shareStr=[NSString stringWithFormat:@"Moji App: Share stickers and mojis with friends."];
    NSArray *items = @[shareStr];
    
    // build an activity view controller
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    
    // and present it
    [self presentViewController:controller animated:YES completion:^{
        // executes after the user selects something
    }];
    
}
- (IBAction)Delete:(id)sender {
    if ([sender tag]==0) {
        if (projectArray.count>0) {
            deleteStr=@"select";
            deleteView.hidden=NO;
        }
    } else {
        if (indexPaths.count>0) {
            
            UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to delete ?" preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            
            UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                deleteView.hidden=YES;
                deleteStr=@"";
                
                NSMutableArray *tempArray=[[NSMutableArray alloc]init];
                tempArray=[indexPaths mutableCopy];
                keyboardDeleteArray=[indexPaths mutableCopy];

                
                [projectArray removeObjectsInArray:tempArray];
                [finalArray removeObjectsInArray:tempArray];
                indexPaths=[[NSMutableArray alloc]init];
                [projectCollec reloadData];

                
                for (int i=0; i<tempArray.count; i++) {
                
                    NSString *removeStr=[[tempArray valueForKey:@"image"] objectAtIndex:i];
                    NSRange range = [removeStr rangeOfString:@"ProjectPictures"];
                    NSRange range1 = [removeStr rangeOfString:@"?alt"];
                    
                    if (range.location != NSNotFound) {
                        removeStr = [removeStr substringWithRange:NSMakeRange(range.location, range1.location-range.location)];
                        removeStr=[removeStr stringByReplacingOccurrencesOfString:@"%2F" withString:@"/"];
                        NSLog(@"%@",removeStr);
                        

                        FIRStorage *storage = [FIRStorage storage];
                        _storageRef = [storage referenceForURL:@"gs://api-project-405149633111.appspot.com"];
                        
                        FIRStorageReference *desertRef = [_storageRef child:removeStr];
                        
                        // Delete the file
                        [desertRef deleteWithCompletion:^(NSError *error){
                            if (error != nil) {
                                NSLog(@"%@",error.localizedDescription) ;
                            } else {
                                NSLog(@"File deleted successfully") ;
                            }
                        }];

                    
                    
                    } else {
                        NSLog(@"= is not found");
                    }

                }
                
                
                _ref = [[FIRDatabase database] reference];
                [[_ref child:@"projects"] setValue:finalArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                    if (error) {
                        NSLog(@"%@",error.localizedDescription);
                        [self alertView:@"" message:error.localizedDescription];
                    } else {
                        popUpBackView.hidden=YES;
                        [projectCollec reloadData];
                       // [self alertView:@"" message:@"Project has been successfully deleted !"];
                        [self update];
                        
                    }
                }];
                
            }];
            
            [alertController addAction:delete];
            [alertController addAction:cancel];
            [self presentViewController:alertController animated:YES completion:nil];
            
        }else {
            [self alertView:@"" message:@"Please select atleast one project to delete !"];
        }
    }
}

-(void)update {
    
    _ref = [[FIRDatabase database] reference];
    
    
    for (int i=0; i<keyboardDeleteArray.count; i++) {
        
        NSString *_projectIdStr=[[keyboardDeleteArray valueForKey:@"projectid"] objectAtIndex:i];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"projectid ==[c] %@",_projectIdStr];
        NSMutableArray *array = [[keyboardArray filteredArrayUsingPredicate:predicate] mutableCopy];
        [keyboardArray removeObjectsInArray:array];
        
    }
    
    
    [[_ref child:@"keyboards"]  setValue:keyboardArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
        if (error) {
            NSLog(@"%@",error.localizedDescription);
            
        } else {
            NSLog(@"success");
        }
    }];
    
}



- (IBAction)Cancel:(id)sender {
    deleteView.hidden=YES;
    deleteStr=@"";
    indexPaths=[[NSMutableArray alloc]init];
    [projectCollec reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)sender {
    [self.keyboardControls setActiveField:sender];
    [self setViewMovedUp12:YES textfieldd:sender];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self setViewMovedUp12:NO textfieldd:textField];
}
#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction {
    UIView *view;
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        view = field.superview.superview;
    } else {
        view = field.superview.superview.superview;
    }
    
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

-(void)setViewMovedUp12:(BOOL)movedUp textfieldd:(UITextField *)textfield {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    CGRect rect = self.view.frame;
    if (movedUp) {
        if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
            rect.origin.y=-105;
        } else if ([[UIScreen mainScreen]bounds].size.height==667) {
            rect.origin.y=-30;
        } 
    } else {
        rect.origin.y=0;
    }
    
    self.view.frame = CGRectMake(0, rect.origin.y, rect.size.width, rect.size.height);
    
    [UIView commitAnimations];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    [descriptionView resignFirstResponder];
}

@end
