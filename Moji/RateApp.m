//
//  RateApp.m
//  Moji
//
//  Created by Brst-Pc109 on 13/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import "RateApp.h"
#import "HCSStarRatingView.h"
@interface RateApp ()

@end

@implementation RateApp

- (void)viewDidLoad {
    [super viewDidLoad];
  
    sureBtn.layer.cornerRadius = 22.0f;
    sureBtn.layer.masksToBounds = NO;
    sureBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    sureBtn.layer.shadowOpacity = 0.3;
    sureBtn.layer.shadowRadius = 2;
    sureBtn.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
}
-(IBAction)Back:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    NSLog(@"Changed rating to %.1f", sender.value);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
