//
//  Register.h
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"
#import "Singleton.h"
#import <AVFoundation/AVFoundation.h>
@import FirebaseAuth;
@import FirebaseDatabase;
@import FirebaseStorage;
@interface Register : UIViewController<BSKeyboardControlsDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    Singleton *singleton;
    IBOutlet UITextField *emailTxt,*userNameTxt,*passwordTxt,*confirmPasswordTxt;
    IBOutlet UIButton *registerBtn,*loginBtn;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UIImageView *userImg;
    NSString *profilePicStr;
    NSMutableArray *myMojisArray,*mojiKeyArray,*myMojiFinalArray;

}
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong, nonatomic) FIRStorageReference *storageRef;

@end
