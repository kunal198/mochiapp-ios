//
//  Register.m
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import "Register.h"
#import "Home.h"
#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface Register ()
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

@end

@implementation Register

- (void)viewDidLoad {
    [super viewDidLoad];
    singleton=[Singleton sharedInstance];
    
    if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
        registerBtn.layer.cornerRadius=20;
        
    } else {
        registerBtn.layer.cornerRadius=22;
        
    }

    registerBtn.clipsToBounds=YES;
    
    
    [emailTxt setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [userNameTxt setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [passwordTxt setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [confirmPasswordTxt setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];

    NSArray *fields = @[userNameTxt,emailTxt,passwordTxt,confirmPasswordTxt];
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];

    indicator.hidden=YES;
    
    profilePicStr=@"no";
    if (![singleton.isLoginstr isEqualToString:@"yes"]) {
        [self fetchMyProjects];
    }
    
}
-(void)fetchMyProjects {
    _ref = [[FIRDatabase database] reference];
    [[_ref child:@"keyboards"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Get user value
        if (snapshot.value != (id)[NSNull null]) {
            myMojisArray=snapshot.value;
            mojiKeyArray=snapshot.value;
            myMojiFinalArray=snapshot.value;
            
            [myMojisArray removeObjectIdenticalTo:[NSNull null]];
            [myMojiFinalArray removeObjectIdenticalTo:[NSNull null]];
            
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"moji_userid ==[c] %@",singleton.uniqueId];
            myMojisArray = [[myMojisArray filteredArrayUsingPredicate:predicate] mutableCopy];
            
            
        } else {
            myMojisArray=[[NSMutableArray alloc]init];
            mojiKeyArray=[[NSMutableArray alloc]init];
            myMojiFinalArray=[[NSMutableArray alloc]init];
            
        }
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
    }];
}


-(IBAction)Register:(id)sender {
    if (emailTxt.text.length && passwordTxt.text.length && userNameTxt.text.length && confirmPasswordTxt.text.length>0) {
        
        
        NSString *trimmed = [userNameTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (trimmed.length>0) {
            BOOL checkEmail= [self NSStringIsValidEmail:emailTxt.text];
            if (checkEmail) {
                
                if ([passwordTxt.text isEqualToString:confirmPasswordTxt.text]) {
                    
                    if ([profilePicStr isEqualToString:@"yes"]) {
                        indicator.hidden=NO;
                        [indicator startAnimating];
                        registerBtn.enabled=NO;
                        loginBtn.enabled=NO;
                        [[FIRAuth auth] createUserWithEmail:emailTxt.text password:passwordTxt.text completion:^(FIRUser *_Nullable user,
                                                                                                                 NSError *_Nullable error) {
                            
                            if (error) {
                                
                                [self alertView:@"" message:error.localizedDescription];
                                
                            } else {
                                
                                singleton.userId=user.uid;
                                singleton.emailStr=emailTxt.text;
                                singleton.userNameStr=userNameTxt.text;
                                
                                [self saveProfileImage];
                                
                                singleton.isLoginstr=@"yes";
                                singleton.isFb=@"no";
                                
                                [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"isLoginstr"];
                                [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"isFb"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                
                                
                                
                                if (myMojisArray.count>0) {
                                    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Previously, you are added some mojis in your keyboard. Do you want to sync that in your account ?" preferredStyle:UIAlertControllerStyleAlert];
                                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                        
                                        NSLog( @"yes");
                                        [myMojiFinalArray removeObjectsInArray:myMojisArray];
                                        
                                        for (int i=0; i<myMojisArray.count; i++) {
                                            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                                            dict=[myMojisArray objectAtIndex:i];
                                            [dict setValue:user.uid forKey:@"moji_userid"];
                                            
                                            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"projectid ==[c] %@",[dict valueForKey:@"projectid"]];
                                            NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"moji_userid ==[c] %@",user.uid];
                                            
                                            
                                            NSCompoundPredicate *predicate=predicate=[NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1,predicate2]];
                                            NSMutableArray *array = [[myMojiFinalArray filteredArrayUsingPredicate:predicate] mutableCopy];
                                            if (array.count==0) {
                                                [myMojiFinalArray addObject:dict];
                                            }
                                            
                                            
                                            
                                            
                                        }
                                        
                                        
                                        [[_ref child:@"keyboards"] setValue:myMojiFinalArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                                            if (error) {
                                                NSLog(@"%@",error.localizedDescription);
                                            }
                                        }];
                                        
                                        
                                        Home *home=[[Home alloc]initWithNibName:@"Home" bundle:nil];
                                        [self.navigationController pushViewController:home animated:YES];
                                        [self alertView:@"" message:@"You have been successfully registered !"];

                                        
                                    }];
                                    
                                    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                        
                                        NSLog( @"cancel");
                                        _ref = [[FIRDatabase database] reference];
                                        [myMojiFinalArray removeObjectsInArray:myMojisArray];
                                        [[_ref child:@"keyboards"]  setValue:myMojiFinalArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                                            if (error) {
                                                NSLog(@"%@",error.localizedDescription);
                                            }
                                        }];
                                        
                                        Home *home=[[Home alloc]initWithNibName:@"Home" bundle:nil];
                                        [self.navigationController pushViewController:home animated:YES];
                                        [self alertView:@"" message:@"You have been successfully registered !"];
                                    }];
                                    
                                    [alertController addAction:cancel];
                                    [alertController addAction:ok];
                                    [self presentViewController:alertController animated:YES completion:nil];
                                    
                                } else {
                                    Home *home=[[Home alloc]initWithNibName:@"Home" bundle:nil];
                                    [self.navigationController pushViewController:home animated:YES];
                                    [self alertView:@"" message:@"You have been successfully registered !"];
                                }
                                

                                
                                
                                
                                
                                
                            }
                            NSLog(@"%@",error.localizedDescription);
                            NSLog(@"%@",user.displayName);
                            indicator.hidden=YES;
                            [indicator stopAnimating];
                            registerBtn.enabled=YES;
                            loginBtn.enabled=YES;

                        }];
                    } else {
                        [self alertView:@"" message:@"Please choose your profile photo !"];
                    }
                    
                    
                    
                } else {
                    [self alertView:@"" message:@"Password does not match with confirm password !"];
                }
            } else {
                [self alertView:@"" message:@"The email address is not valid."];
            }

        } else {
            [self alertView:@"" message:@"Enter the username !"];
        }
        
        
     } else {
         [self alertView:@"" message:@"Enter all fields !"];
    }
}

- (IBAction)takePhoto:(UIButton *)sender {
 
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [self checkCameraAuthorization];
    }];
    
    UIAlertAction *gallery = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [self checkPhotoAuthorization];
    }];
    
 //   [camera setValue:[[UIImage imageNamed:@"camera_3"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
 //   [gallery setValue:[[UIImage imageNamed:@"image_3"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    [alertController addAction:gallery];
    [alertController addAction:camera];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
}


-(void)checkPhotoAuthorization {
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if (status == PHAuthorizationStatusAuthorized) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];

    }
    
    else if (status == PHAuthorizationStatusDenied) {
        
        
        [self accessMethod:@"Please go to Settings and enable the photos for this app to use this feature."];
        
    }
    
    else if (status == PHAuthorizationStatusNotDetermined) {
        
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:picker animated:YES completion:NULL];

            }
            
            else {
                [self accessMethod:@"Please go to Settings and enable the photos for this app to use this feature."];
            }
        }];
    }
    
    else if (status == PHAuthorizationStatusRestricted) {
        // Restricted access - normally won't happen.
    }
}

-(void) checkCameraAuthorization {
    
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(status == AVAuthorizationStatusAuthorized) { // authorized
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];

    }
    else if(status == AVAuthorizationStatusDenied){ // denied
        if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                // until iOS 8. So for iOS 7 permission will always be granted.
                
                NSLog(@"DENIED");
                
                if (granted) {
                    // Permission has been granted. Use dispatch_async for any UI updating
                    // code because this block may be executed in a thread.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                        picker.delegate = self;
                        picker.allowsEditing = YES;
                        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        [self presentViewController:picker animated:YES completion:NULL];

                    });
                } else {
                    // Permission has been denied.
                    [self accessMethod:@"Please go to Settings and enable the camera for this app to use this feature."];
                    
                }
            }];
        }
    }
    else if(status == AVAuthorizationStatusRestricted){ // restricted
        [self accessMethod:@"Please go to Settings and enable the camera for this app to use this feature."];
        
    }
    else if(status == AVAuthorizationStatusNotDetermined){ // not determined
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){ // Access has been granted ..do something
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:picker animated:YES completion:NULL];

            } else { // Access denied ..do something
                [self accessMethod:@"Please go to Settings and enable the camera for this app to use this feature."];
            }
        }];
    }
}

-(void)accessMethod:(NSString *)message {
    
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"Not Authorized" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction *Settings = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    
    [alertController addAction:Settings];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    profilePicStr=@"yes";
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    userImg.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    userImg.layer.cornerRadius=userImg.frame.size.width/2;
    userImg.clipsToBounds=YES;
}

- (void) saveProfileImage {
    
    NSString *username = userNameTxt.text;
    
    if (userImg.image != nil) {
        FIRStorage *storage = [FIRStorage storage];
        _storageRef = [storage referenceForURL:@"gs://api-project-405149633111.appspot.com"];
   //     _storageRef = [storage referenceForURL:@"gs://moji-159605.appspot.com"];
        
        NSString *imageID = [[NSUUID UUID] UUIDString];
        NSString *imageName = [NSString stringWithFormat:@"MojiPictures/%@.jpg",imageID];
        FIRStorageReference *profilePicRef = [_storageRef child:imageName];
        FIRStorageMetadata *metadata = [[FIRStorageMetadata alloc] init];
        metadata.contentType = @"image/jpeg";
        NSData *imageData = UIImageJPEGRepresentation(userImg.image, 0.3);
        [profilePicRef putData:imageData metadata:metadata completion:^(FIRStorageMetadata *metadata, NSError *error) {
             if (!error) {
                NSString *profileImageURL = metadata.downloadURL.absoluteString;

                 singleton.userImgStr=profileImageURL;

                 
                 [[NSUserDefaults standardUserDefaults] setObject:singleton.userNameStr forKey:@"userNameStr"];
                 [[NSUserDefaults standardUserDefaults] setObject:singleton.userImgStr forKey:@"userImgStr"];
                 [[NSUserDefaults standardUserDefaults] setObject:singleton.emailStr forKey:@"emailStr"];
                 [[NSUserDefaults standardUserDefaults] setObject:[FIRAuth auth].currentUser.uid forKey:@"userId"];
                 [[NSUserDefaults standardUserDefaults] synchronize];

                 [[NSNotificationCenter defaultCenter] postNotificationName:@"getNotification" object:self];

                 
                  _ref = [[FIRDatabase database] reference];
                 [[[_ref child:@"users"] child:[FIRAuth auth].currentUser.uid] setValue:@{@"Name": username, @"Profile Picture":profileImageURL}];
                                                                   

             }
             else if (error)
             {
                 NSLog(@"Failed to Register User with profile image");
             }
         }];
    }
}


-(void) alertView:(NSString *)title message:(NSString *)messageStr {
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:title message:messageStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


- (IBAction)Back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)sender {
    
    [self.keyboardControls setActiveField:sender];
    
    if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
        if ([sender isEqual:userNameTxt]){
            [self setViewMovedUp12:YES textfieldd:sender];
        }
    }
    
       if ([sender isEqual:passwordTxt] || [sender isEqual:confirmPasswordTxt] || [sender isEqual:emailTxt]){
        [self setViewMovedUp12:YES textfieldd:sender];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self setViewMovedUp12:NO textfieldd:textField];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    [emailTxt resignFirstResponder];
    [passwordTxt resignFirstResponder];
    [confirmPasswordTxt resignFirstResponder];
    [userNameTxt resignFirstResponder];

}

#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction {
    UIView *view;
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        view = field.superview.superview;
    } else {
        view = field.superview.superview.superview;
    }
    
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}




-(void)setViewMovedUp12:(BOOL)movedUp textfieldd:(UITextField *)textfield {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    CGRect rect = self.view.frame;
    if (movedUp) {
        
        if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
            if ([textfield isEqual:userNameTxt]){
                rect.origin.y=-30;
            } else if ([textfield isEqual:emailTxt]){
                rect.origin.y=-85;
            } else if ([textfield isEqual:passwordTxt]){
                rect.origin.y=-100;
            } else if ([textfield isEqual:confirmPasswordTxt]){
                rect.origin.y=-150;
            }

        } else if ([[UIScreen mainScreen]bounds].size.height==667) {
        if ([textfield isEqual:emailTxt]){
                rect.origin.y=-60;
            } else if ([textfield isEqual:passwordTxt]){
                rect.origin.y=-80;
            } else if ([textfield isEqual:confirmPasswordTxt]){
                rect.origin.y=-140;
            }
    
        } else {
        
            if ([textfield isEqual:emailTxt]){
                rect.origin.y=-40;
            } else if ([textfield isEqual:passwordTxt]){
                rect.origin.y=-70;
            } else if ([textfield isEqual:confirmPasswordTxt]){
                rect.origin.y=-130;
            }

        }
        
    }
    else {
        rect.origin.y=0;
    }
    
    self.view.frame = CGRectMake(0, rect.origin.y, rect.size.width, rect.size.height);
    
    [UIView commitAnimations];
}


@end
