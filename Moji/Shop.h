//
//  Shop.h
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
@import FirebaseDatabase;

@interface Shop : UIViewController
{
    Singleton *singletonn;
    IBOutlet UITableView *shopTbl;
    NSMutableArray *mojiArray,*projectArray,*keyArray,*freeArray,*myMojisArray,*myMojiFinalArray,*mojiKeyArray,*reportArray;
    IBOutlet UIView *backView,*popUpView,*popupBackView,*backView1;
    IBOutlet UIButton *freeBtn,*paidBtn,*mojisBtn,*downloadBtn,*freeBtn1,*paidBtn1,*downloadImgBtn,*flagImgBtn;
    IBOutlet UILabel *freelbl,*paidLbl,*mojisLbl,*freelbl1,*paidLbl1,*mojiUserNameLbl;
    IBOutlet UIImageView *mojiUserImg;
    IBOutlet UICollectionView *mojisCollec;
    NSString *statusStr;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UILabel *alertLbl,*downloadLbl,*flagLbl,*mojiTitlelbl;
    NSString *userId;
}
@property (strong, nonatomic) FIRDatabaseReference *ref;

@end
