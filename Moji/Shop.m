//
//  Shop.m
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import "Shop.h"
#import "ShopCell.h"
#import "ShopMojiCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface Shop ()<UICollectionViewDelegate,UICollectionViewDataSource>

@end

@implementation Shop

- (void)viewDidLoad {
    [super viewDidLoad];
    singletonn=[Singleton sharedInstance];
    
        if ([singletonn.isLoginstr isEqualToString:@"yes"]) {
            userId=singletonn.userId;
        } else {
            userId=singletonn.uniqueId;
        }
    
    backView.hidden=NO;
    
    
    statusStr=@"free";
    mojiArray=[[NSMutableArray alloc]initWithObjects:@"Vector Smart Object",@"Vector Smart Object2",@"Vector Smart Object3",@"Vector Smart Object4",@"Vector Smart Object5",@"Vector Smart Object6",@"Vector Smart Object3",@"Vector Smart Object",@"Vector Smart Object4",@"Vector Smart Object2",@"Vector Smart Object6",@"Vector Smart Object5",@"Vector Smart Object",@"Vector Smart Object2",@"Vector Smart Object3",@"Vector Smart Object4",@"Vector Smart Object5",@"Vector Smart Object6",@"Vector Smart Object3",@"Vector Smart Object",@"Vector Smart Object4",@"Vector Smart Object2",@"Vector Smart Object6",@"Vector Smart Object5", nil];
    
    projectArray=[[NSMutableArray alloc]initWithObjects:@"Mojis",@"Love",@"Valentine",@"Birthday",@"Games",@"Happy",@"Sad", nil];
    
    backView.layer.cornerRadius=18;
    backView.layer.borderWidth=1;
    backView.layer.borderColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1].CGColor;
    backView.clipsToBounds=YES;
    
    backView1.layer.cornerRadius=18;
    backView1.layer.borderWidth=1;
    backView1.layer.borderColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1].CGColor;
    backView1.clipsToBounds=YES;
    
    
    popUpView.layer.cornerRadius=10;
    popUpView.clipsToBounds=YES;
    
    mojiUserImg.layer.cornerRadius=mojiUserImg.frame.size.height/2;
    mojiUserImg.clipsToBounds=YES;

    
    downloadBtn.layer.cornerRadius=18;
    downloadBtn.clipsToBounds=YES;
    popupBackView.hidden=YES;
    
    [mojisCollec registerNib:[UINib nibWithNibName:@"ShopMojiCell" bundle:nil] forCellWithReuseIdentifier:@"ShopMojiCell"];
    [indicator startAnimating];
    [self fetchProjects];
    [self fetchMyProjects];
    [self reportProjects];
}


-(void)fetchProjects {
    _ref = [[FIRDatabase database] reference];
    [[_ref child:@"projects"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Get user value
        if (snapshot.value != (id)[NSNull null]) {
            keyArray=snapshot.value;
            freeArray=snapshot.value;
            [freeArray removeObjectIdenticalTo:[NSNull null]];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status ==[c] %@",@"1"];
            freeArray = [[freeArray filteredArrayUsingPredicate:predicate] mutableCopy];
            
            
            
            NSMutableArray *tempArray=[[NSMutableArray alloc]init];
            tempArray=[freeArray mutableCopy];
            for (int i=0; i<freeArray.count; i++) {
                
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[freeArray objectAtIndex:i];
                NSArray *mojisArray=[[freeArray valueForKey:@"mojis"] objectAtIndex:i];
                
                if ([mojisArray isKindOfClass:[NSString class]]) {
                    [tempArray removeObject:dict];
                }
            }
            
            freeArray=tempArray;
            
        } else {
            projectArray=[[NSMutableArray alloc]init];
            keyArray=[[NSMutableArray alloc]init];
        }
        
        
        if ([statusStr isEqualToString:@"free"]) {
            alertLbl.text=@"There is no mojis in free section !";
            if (freeArray.count==0) {
                alertLbl.hidden=NO;
            } else {
                alertLbl.hidden=YES;
            }
            
        }
        
        [shopTbl reloadData];
        indicator.hidden=YES;
        [indicator stopAnimating];
        
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        indicator.hidden=YES;
        [indicator stopAnimating];
        
    }];
}
-(void)fetchMyProjects {
    _ref = [[FIRDatabase database] reference];
    [[_ref child:@"keyboards"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Get user value
        if (snapshot.value != (id)[NSNull null]) {
            myMojisArray=snapshot.value;
            mojiKeyArray=snapshot.value;
            myMojiFinalArray=snapshot.value;
            
            [myMojisArray removeObjectIdenticalTo:[NSNull null]];
            [myMojiFinalArray removeObjectIdenticalTo:[NSNull null]];
            
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"moji_userid ==[c] %@",userId];
            myMojisArray = [[myMojisArray filteredArrayUsingPredicate:predicate] mutableCopy];
            
            
            
            NSMutableArray *tempArray=[[NSMutableArray alloc]init];
            tempArray=[myMojisArray mutableCopy];
            for (int i=0; i<myMojisArray.count; i++) {
                
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[myMojisArray objectAtIndex:i];
                NSArray *mojisArray=[[myMojisArray valueForKey:@"mojis"] objectAtIndex:i];
                
                if ([mojisArray isKindOfClass:[NSString class]]) {
                    [tempArray removeObject:dict];
                }
            }
            
            myMojisArray=tempArray;

            
        } else {
            myMojisArray=[[NSMutableArray alloc]init];
            mojiKeyArray=[[NSMutableArray alloc]init];
            myMojiFinalArray=[[NSMutableArray alloc]init];
            
        }
        
        if ([statusStr isEqualToString:@"moji"]) {
            alertLbl.text=@"There is no mojis in \"My Mojis\" section !";
            if (myMojisArray.count==0) {
                alertLbl.hidden=NO;
            } else {
                alertLbl.hidden=YES;
            }
            
        }
        
        [shopTbl reloadData];
        
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        
    }];
}


-(void)reportProjects {
    _ref = [[FIRDatabase database] reference];
    [[_ref child:@"reports"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Get user value
        if (snapshot.value != (id)[NSNull null]) {
            reportArray=snapshot.value;
            [reportArray removeObjectIdenticalTo:[NSNull null]];
        } else {
            reportArray=[[NSMutableArray alloc]init];
        }
        
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        
    }];
}



-(IBAction)Back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)PopupCross:(id)sender {
    popupBackView.hidden=YES;
}
-(IBAction)Free:(id)sender {
    statusStr=@"free";
    
    freeBtn.backgroundColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    freelbl.textColor=[UIColor whiteColor];
    
    paidBtn.backgroundColor=[UIColor clearColor];
    paidLbl.textColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    
    freeBtn1.backgroundColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    freelbl1.textColor=[UIColor whiteColor];
    
    paidBtn1.backgroundColor=[UIColor clearColor];
    paidLbl1.textColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    
    
    mojisBtn.backgroundColor=[UIColor clearColor];
    mojisLbl.textColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    
    [shopTbl reloadData];
    
    alertLbl.text=@"There is no mojis in free section !";
    if (freeArray.count==0) {
        alertLbl.hidden=NO;
    } else {
        alertLbl.hidden=YES;
    }
    
}
-(IBAction)Paid:(id)sender {
    
    alertLbl.hidden=YES;
    statusStr=@"paid";
    
    freeBtn.backgroundColor=[UIColor clearColor];
    freelbl.textColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    
    paidBtn.backgroundColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    paidLbl.textColor=[UIColor whiteColor];
    
    freeBtn1.backgroundColor=[UIColor clearColor];
    freelbl1.textColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    
    paidBtn1.backgroundColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    paidLbl1.textColor=[UIColor whiteColor];
    
    
    mojisBtn.backgroundColor=[UIColor clearColor];
    mojisLbl.textColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    [shopTbl reloadData];
    
    
}
-(IBAction)MyMojis:(id)sender {
    statusStr=@"moji";
    
    freeBtn.backgroundColor=[UIColor clearColor];
    freelbl.textColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    
    paidBtn.backgroundColor=[UIColor clearColor];
    paidLbl.textColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    
    mojisBtn.backgroundColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    mojisLbl.textColor=[UIColor whiteColor];
    [shopTbl reloadData];
    
    alertLbl.text=@"There is no mojis in \"My Mojis\" section !";
    if (myMojisArray.count==0) {
        alertLbl.hidden=NO;
    } else {
        alertLbl.hidden=YES;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([statusStr isEqualToString:@"free"]) {
        return freeArray.count;
    } else if ([statusStr isEqualToString:@"paid"]) {
        return projectArray.count;
    }
    return myMojisArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 111;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ShopCell *cell = (ShopCell *)[tableView dequeueReusableCellWithIdentifier:@"ShopCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShopCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.btn.layer.cornerRadius=13;
    cell.btn.clipsToBounds=YES;
    cell.addtoKeyboardLbl.hidden=YES;
    cell.deleteBtn.hidden=YES;
    cell.backView.hidden=NO;
    [cell.btn setBackgroundColor:[UIColor colorWithRed:148.0f/255 green:218.0f/255 blue:80.0f/255 alpha:1]];
    cell.btn.titleLabel.font=[UIFont systemFontOfSize:12];
    
    
    [cell.flagBtn addTarget:self action:@selector(Flag:) forControlEvents:UIControlEventTouchUpInside];
    cell.flagBtn.tag=indexPath.row;

    
    if ([statusStr isEqualToString:@"free"]) {
        cell.nameLbl.text=[[freeArray objectAtIndex:indexPath.row] valueForKey:@"name"];
        cell.downloadLbl.text=[NSString stringWithFormat:@"%@",[[freeArray objectAtIndex:indexPath.row] valueForKey:@"download"]];
        cell.flagLbl.text=[NSString stringWithFormat:@"%@",[[freeArray objectAtIndex:indexPath.row] valueForKey:@"flag"]];
        
        cell.btn.tag=indexPath.row;
        [cell.btn addTarget:self action:@selector(OpenPopUp:) forControlEvents:UIControlEventTouchUpInside];
        [cell.downloadBtn addTarget:self action:@selector(OpenPopUp:) forControlEvents:UIControlEventTouchUpInside];
        cell.downloadBtn.tag=indexPath.row;
        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"userid ==[c] %@",[[freeArray valueForKey:@"userid"] objectAtIndex:indexPath.row]];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"name ==[c] %@",[[freeArray valueForKey:@"name"] objectAtIndex:indexPath.row]];
        NSCompoundPredicate *predicate=predicate=[NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1,predicate2]];
        NSMutableArray *array = [[myMojisArray filteredArrayUsingPredicate:predicate] mutableCopy];
        if (array.count>0) {
            [cell.btn setTitle:@"Downloaded" forState:UIControlStateNormal];
            [cell.btn setBackgroundColor:[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1]];
            cell.btn.titleLabel.font=[UIFont systemFontOfSize:11];

        } else {
            [cell.btn setTitle:@"Free" forState:UIControlStateNormal];
        }
        
        
    } else if ([statusStr isEqualToString:@"paid"]) {
        cell.nameLbl.text=[projectArray objectAtIndex:indexPath.row];
        
        [cell.btn setTitle:@"$3.45" forState:UIControlStateNormal];
        cell.btn.tag=indexPath.row;
        [cell.btn addTarget:self action:@selector(OpenPopUp:) forControlEvents:UIControlEventTouchUpInside];
        [cell.downloadBtn addTarget:self action:@selector(OpenPopUp:) forControlEvents:UIControlEventTouchUpInside];
        cell.downloadBtn.tag=indexPath.row;
        
        
    } else if ([statusStr isEqualToString:@"moji"]) {
        cell.nameLbl.text=[[myMojisArray objectAtIndex:indexPath.row] valueForKey:@"name"];
        
        cell.backView.hidden=YES;
        [cell.btn setTitle:@"" forState:UIControlStateNormal];
        cell.addtoKeyboardLbl.hidden=NO;
        [cell.btn addTarget:self action:@selector(AddToKeyboard:) forControlEvents:UIControlEventTouchUpInside];
        cell.btn.tag=indexPath.row;
        
        if ([[[myMojisArray valueForKey:@"moji_status"] objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            cell.addtoKeyboardLbl.text=@"Add to Keyboard";
        }else {
            cell.addtoKeyboardLbl.text=@"Remove from Keyboard";
        }
        
        cell.deleteBtn.hidden=NO;
        [cell.deleteBtn addTarget:self action:@selector(DeleteMoji:) forControlEvents:UIControlEventTouchUpInside];
        cell.deleteBtn.tag=indexPath.row;
    }
    
    cell.mojiCollec.delegate=self;
    cell.mojiCollec.dataSource=self;
    [cell.mojiCollec registerNib:[UINib nibWithNibName:@"ShopMojiCell" bundle:nil] forCellWithReuseIdentifier:@"ShopMojiCell"];
    cell.mojiCollec.tag=indexPath.row;
    [cell.mojiCollec reloadData];
    return cell;
}


-(IBAction)Flag:(UIButton *)sender {
    if ([statusStr isEqualToString:@"free"]){
        
        if ([[[freeArray valueForKey:@"userid"] objectAtIndex:[sender tag]] isEqualToString:userId]) {
             [self alertView:@"" message:@"You can't report your own moji."];
        } else {
        
        
        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"userid ==[c] %@",userId];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"projectid ==[c] %@",[[freeArray valueForKey:@"projectid"] objectAtIndex:[sender tag]]];
        
        
        NSCompoundPredicate *predicate=predicate=[NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1,predicate2]];
        NSMutableArray *array = [[reportArray filteredArrayUsingPredicate:predicate] mutableCopy];
        if (array.count>0) {
            [self alertView:@"" message:@"You are already reported to this mojis."];
            
        } else {
            UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to report?" preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            
            UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Report" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                
                NSDictionary *dict1=[freeArray objectAtIndex:[sender tag]];
                NSInteger anIndex=[keyArray indexOfObject:dict1];
                if(NSNotFound == anIndex) {
                    NSLog(@"not found");
                }
                
                
                NSInteger fVal=[[dict1 objectForKey:@"flag"] integerValue];
                fVal+=1;
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                [dict setValue:[NSString stringWithFormat:@"%ld",(long)fVal] forKey:@"flag"];
                
                [indicator startAnimating];
                indicator.hidden=NO;
                
                
                _ref = [[FIRDatabase database] reference];
                [[[_ref child:@"projects"] child:[NSString stringWithFormat:@"%ld",(long)anIndex]] updateChildValues:dict withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                    if (error) {
                        NSLog(@"%@",error.localizedDescription);
                        [self alertView:@"" message:error.localizedDescription];
                    }else {
                        
                        flagLbl.text=[NSString stringWithFormat:@"%ld",fVal];
                        
                        NSMutableDictionary *reportDict=[[NSMutableDictionary alloc]init];
                        [reportDict setValue:userId forKey:@"userid"];
                        [reportDict setValue:[dict1 objectForKey:@"projectid"] forKey:@"projectid"];
                        [reportArray addObject:reportDict];
                        
                        [[_ref child:@"reports"]  setValue:reportArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                            if (error) {
                                NSLog(@"%@",error.localizedDescription);
                                [self alertView:@"" message:error.localizedDescription];
                            } else {
                                
                              //  [self alertView:@"" message:@"You have been successfully reported to this mojis."];
                                
                            }
                            [indicator stopAnimating];
                            indicator.hidden=YES;
                            
                        }];
                        
                        [shopTbl reloadData];
                        
                    }
                    [shopTbl reloadData];
                }];
                
                
            }];
            
            [alertController addAction:delete];
            [alertController addAction:cancel];
            [self presentViewController:alertController animated:YES completion:nil];

        }
        
        }
 
    }
}


-(IBAction)LikeMoji:(UIButton *)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Work-in-progress" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}


-(IBAction)DeleteMoji:(UIButton *)sender {
    
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to delete ?" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        
        _ref = [[FIRDatabase database] reference];
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        dict=[myMojisArray objectAtIndex:[sender tag]];
        [myMojisArray removeObjectAtIndex:[sender tag]];
        [myMojiFinalArray removeObject:dict];
        [[_ref child:@"keyboards"]  setValue:myMojiFinalArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
            if (error) {
                NSLog(@"%@",error.localizedDescription);
                [self alertView:@"" message:error.localizedDescription];
            } else {
                
            //    [self alertView:@"" message:@"Moji has been successfully removed from \"My Mojis\""];
                
            }
            [indicator stopAnimating];
            indicator.hidden=YES;
            
        }];
        
        
    }];
    
    [alertController addAction:delete];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(IBAction)AddToKeyboard:(UIButton *)sender {
    if ([statusStr isEqualToString:@"moji"]) {
        NSDictionary *dict1=[myMojisArray objectAtIndex:[sender tag]];
        NSInteger anIndex=[mojiKeyArray indexOfObject:dict1];
        if(NSNotFound == anIndex) {
            NSLog(@"not found");
        }
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        if ([[[myMojisArray valueForKey:@"moji_status"] objectAtIndex:[sender tag]] isEqualToString:@"0"]) {
            [dict setValue:@"1" forKey:@"moji_status"];
        }else {
            [dict setValue:@"0" forKey:@"moji_status"];
        }
        
        [indicator startAnimating];
        indicator.hidden=NO;
        _ref = [[FIRDatabase database] reference];
        [[[_ref child:@"keyboards"]  child:[NSString stringWithFormat:@"%ld",(long)anIndex]] updateChildValues:dict withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
            if (error) {
                NSLog(@"%@",error.localizedDescription);
                [self alertView:@"" message:error.localizedDescription];
            }else {
                if ([[[myMojisArray valueForKey:@"moji_status"] objectAtIndex:[sender tag]] isEqualToString:@"0"]) {
                  //  [self alertView:@"" message:@"Moji has been successfully removed to keyboard !"];
                }else {
                 //   [self alertView:@"" message:@"Moji has been successfully added to keyboard !"];
                }
            }
            [indicator stopAnimating];
            indicator.hidden=YES;
            [shopTbl reloadData];
        }];
        
    }
}
-(IBAction)OpenPopUp:(UIButton *)sender {
    if ([statusStr isEqualToString:@"paid"]) {
        popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
        [UIView animateWithDuration:0.3/1.5 animations:^{
            popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3/2 animations:^{
                    popUpView.transform = CGAffineTransformIdentity;
                }];
            }];
        }];
        [mojisCollec reloadData];
        popupBackView.hidden=NO;
    } else if ([statusStr isEqualToString:@"free"]){
        
        
        
        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"userid ==[c] %@",[[freeArray valueForKey:@"userid"] objectAtIndex:[sender tag]]];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"name ==[c] %@",[[freeArray valueForKey:@"name"] objectAtIndex:[sender tag]]];
        
        
        NSCompoundPredicate *predicate=predicate=[NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1,predicate2]];
        NSMutableArray *array = [[myMojisArray filteredArrayUsingPredicate:predicate] mutableCopy];
        if (array.count>0) {
          //   [self alertView:@"" message:@"Moji is already downloaded to \"My Mojis\""];
            
        } else {
            
            
            [downloadBtn setBackgroundColor:[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1]];
            [downloadBtn removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [downloadBtn setTitle:@"Downloaded" forState:UIControlStateNormal];
            [downloadImgBtn removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];

            
            
            NSDictionary *dict1=[freeArray objectAtIndex:[sender tag]];
            NSInteger anIndex=[keyArray indexOfObject:dict1];
            if(NSNotFound == anIndex) {
                NSLog(@"not found");
            }
            
            
            NSInteger dVal=[[dict1 objectForKey:@"download"] integerValue];
            NSInteger fVal=[[dict1 objectForKey:@"flag"] integerValue];
            dVal+=1;
           
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:[NSString stringWithFormat:@"%ld",(long)dVal] forKey:@"download"];
            [dict setValue:[NSString stringWithFormat:@"%ld",(long)fVal] forKey:@"flag"];
            
            [indicator startAnimating];
            indicator.hidden=NO;
            
             self.view.userInteractionEnabled=false;
            _ref = [[FIRDatabase database] reference];
            [[[_ref child:@"projects"] child:[NSString stringWithFormat:@"%ld",(long)anIndex]] updateChildValues:dict withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                if (error) {
                    NSLog(@"%@",error.localizedDescription);
                    [self alertView:@"" message:error.localizedDescription];
                }else {
                    
                    
                    NSMutableDictionary *addDict1=[NSMutableDictionary dictionaryWithObjectsAndKeys:@"free", @"moji_type",
                                                   @"0", @"moji_status",userId, @"moji_userid",
                                                   nil] ;
                    
                    
                    [addDict1 addEntriesFromDictionary:dict1];
                    [myMojisArray addObject:addDict1];
                    [myMojiFinalArray addObject:addDict1];
                    
                    [[_ref child:@"keyboards"] setValue:myMojiFinalArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                        if (error) {
                            NSLog(@"%@",error.localizedDescription);
                            [self alertView:@"" message:error.localizedDescription];
                        } else {
                            
                        //    [self alertView:@"" message:@"Moji has been successfully downloaded to \"My Mojis\""];
                            
                        }
                        [indicator stopAnimating];
                        indicator.hidden=YES;
                        
                    }];
                    
                     self.view.userInteractionEnabled=true;
                    [shopTbl reloadData];
                    
                }
                [indicator stopAnimating];
                indicator.hidden=YES;
                [shopTbl reloadData];
            }];
        }
    }
}

-(void) alertView:(NSString *)title message:(NSString *)messageStr {
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:title message:messageStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if ([statusStr isEqualToString:@"free"]) {
        NSArray *mojisArray;
        mojisArray=[[freeArray valueForKey:@"mojis"] objectAtIndex:collectionView.tag];
        
        if ([mojisArray isKindOfClass:[NSString class]]) {
            mojisArray=[[NSMutableArray alloc]init];
        }
        return mojisArray.count;
    } else if ([statusStr isEqualToString:@"moji"]) {
        NSArray *mojisArray;
        mojisArray=[[myMojisArray valueForKey:@"mojis"] objectAtIndex:collectionView.tag];
        
        if ([mojisArray isKindOfClass:[NSString class]]) {
            mojisArray=[[NSMutableArray alloc]init];
        }
        return mojisArray.count;
    }
    return mojiArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ShopMojiCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ShopMojiCell" forIndexPath:indexPath];
    NSArray *mojisArray;
    if ([statusStr isEqualToString:@"free"]) {
        mojisArray=[[freeArray valueForKey:@"mojis"] objectAtIndex:collectionView.tag];
        [cell.mojiImg sd_setImageWithURL:[NSURL URLWithString:[[mojisArray objectAtIndex:indexPath.row]valueForKey:@"image"]]];
    } else if ([statusStr isEqualToString:@"moji"]) {
        mojisArray=[[myMojisArray valueForKey:@"mojis"] objectAtIndex:collectionView.tag];
        [cell.mojiImg sd_setImageWithURL:[NSURL URLWithString:[[mojisArray objectAtIndex:indexPath.row]valueForKey:@"image"]]];
    } else {
        cell.mojiImg.image=[UIImage imageNamed:[mojiArray objectAtIndex:indexPath.row]];
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([statusStr isEqualToString:@"free"]) {
        mojiUserNameLbl.text=[NSString stringWithFormat:@"by %@",[[freeArray valueForKey:@"username"] objectAtIndex:collectionView.tag]];
        [mojiUserImg sd_setImageWithURL:[NSURL URLWithString:[[freeArray objectAtIndex:collectionView.tag]valueForKey:@"userimage"]] placeholderImage:[UIImage imageNamed:@"user"]];
        downloadLbl.text=[NSString stringWithFormat:@"%@",[[freeArray objectAtIndex:collectionView.tag] valueForKey:@"download"]];
        flagLbl.text=[NSString stringWithFormat:@"%@",[[freeArray objectAtIndex:collectionView.tag] valueForKey:@"flag"]];
        mojiTitlelbl.text=[[freeArray objectAtIndex:collectionView.tag] valueForKey:@"name"];


        
        
        NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:collectionView.tag inSection:0];
        ShopCell *cell = (ShopCell *)[shopTbl cellForRowAtIndexPath:indexPath1];
        NSLog(@"button is %@ at index %ld",cell.btn.currentTitle,indexPath1.row);
      
        [flagImgBtn addTarget:self action:@selector(Flag:) forControlEvents:UIControlEventTouchUpInside];
        flagImgBtn.tag=cell.flagBtn.tag;

        
        [downloadBtn setTitle:cell.btn.currentTitle forState:UIControlStateNormal];
        downloadBtn.tag=cell.btn.tag;
        downloadImgBtn.tag=cell.btn.tag;
        if ([downloadBtn.currentTitle isEqualToString:@"Free"]) {
            [downloadBtn setBackgroundColor:[UIColor colorWithRed:148.0f/255 green:218.0f/255 blue:80.0f/255 alpha:1]];
            [downloadBtn addTarget:self action:@selector(OpenPopUp:) forControlEvents:UIControlEventTouchUpInside];
            [downloadImgBtn addTarget:self action:@selector(OpenPopUp:) forControlEvents:UIControlEventTouchUpInside];
        } else if ([downloadBtn.currentTitle isEqualToString:@"Downloaded"]) {
            [downloadBtn setBackgroundColor:[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1]];
            [downloadBtn removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [downloadImgBtn removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];

        }

        popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
        [UIView animateWithDuration:0.3/1.5 animations:^{
            popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3/2 animations:^{
                    popUpView.transform = CGAffineTransformIdentity;
                }];
            }];
        }];
        mojisCollec.tag=collectionView.tag;
        [mojisCollec reloadData];
        popupBackView.hidden=NO;
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[UIScreen mainScreen]bounds].size.height==736) {
        return CGSizeMake(56, 56);
    } else {
        return CGSizeMake(50, 50);
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
