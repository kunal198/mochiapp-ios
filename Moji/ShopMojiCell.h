//
//  ShopMojiCell.h
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FLAnimatedImage/FLAnimatedImageView.h>

@interface ShopMojiCell : UICollectionViewCell
@property(nonatomic,strong)IBOutlet FLAnimatedImageView *mojiImg;

@end
