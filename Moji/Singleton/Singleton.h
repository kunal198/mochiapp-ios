//
//  Singleton.h
//  Moji
//
//  Created by Brst-Pc109 on 14/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject
@property(nonatomic,strong)NSString *emailStr,*isLoginstr,*userNameStr,*userImgStr,*userId,*isFb,*uniqueId;

+ (instancetype)sharedInstance;
@end
