//
//  Singleton.m
//  Moji
//
//  Created by Brst-Pc109 on 14/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

static Singleton* _sharedMySingleton = nil;

+ (instancetype)sharedInstance {
    @synchronized([Singleton class])
    {
        if (!_sharedMySingleton)
            _sharedMySingleton = [[self alloc] init];
        
        return _sharedMySingleton;
    }
    
    return nil;
}


@end
