//
//  SubCategories.h
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"
#import "Singleton.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
@import FirebaseStorage;
@import FirebaseDatabase;

@interface SubCategories : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,BSKeyboardControlsDelegate>
{
    Singleton *singletonn;
    IBOutlet UICollectionView *projectCollec;
    NSMutableArray *indexPaths;
    NSString *deleteStr;
    IBOutlet UIView *deleteView;
    
    IBOutlet UIView *popUpView,*popUpBackView;
    IBOutlet UITextField *descriptionView;
    IBOutlet UIButton *UploadBtn,*deleteBtn;
    UIImage *projectImg;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UILabel *titleLbl;
    NSString *typeStr;
    NSString *gifPath;
    NSURL *videoUrl;
    NSData *gifData;
    
    NSMutableArray *keyboardArray;
    NSString *isCamera;

}
@property (nonatomic,strong)NSMutableArray *projectArray;
@property (nonatomic,strong)NSString *titleStr,*keyStr,*projectIdStr;
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong, nonatomic) FIRStorageReference *storageRef;

@end
