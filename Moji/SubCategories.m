//
//  SubCategories.m
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import "SubCategories.h"
#import "ProjectCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <FLAnimatedImage/FLAnimatedImage.h>
#import "NSGIF.h"
#import <VideoEditor/VideoEditorVC.h>
#import <AdobeCreativeSDKImage/AdobeCreativeSDKImage.h>

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface SubCategories ()<VideoEditorSDKDelegate,AdobeUXImageEditorViewControllerDelegate>
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

@end

@implementation SubCategories

- (void)viewDidLoad {
    [super viewDidLoad];
    singletonn=[Singleton sharedInstance];
    
    titleLbl.text=_titleStr;
    
    [projectCollec registerNib:[UINib nibWithNibName:@"ProjectCell" bundle:nil] forCellWithReuseIdentifier:@"ProjectCell"];
    
    if ([_projectArray isKindOfClass:[NSString class]]) {
        NSLog(@"string");
        _projectArray=[[NSMutableArray alloc]init];
    }
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, descriptionView.frame.size.height)];
    descriptionView.leftView = paddingView;
    descriptionView.leftViewMode = UITextFieldViewModeAlways;
    
    popUpView.layer.cornerRadius=10;
    popUpView.clipsToBounds=YES;
    
    UploadBtn.layer.cornerRadius=18;
    UploadBtn.clipsToBounds=YES;
    popUpBackView.hidden=YES;
    
    
    UIColor *color = [UIColor lightGrayColor];
    color = [color colorWithAlphaComponent:0.4f];
    
    descriptionView.layer.cornerRadius=6;
    descriptionView.layer.borderWidth=1.4;
    descriptionView.layer.borderColor=color.CGColor;
    descriptionView.clipsToBounds=YES;
    
    NSArray *fields = @[ descriptionView];
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    
    [indicator stopAnimating];
    indicator.hidden=YES;
    
    [self fetchMyProjects];
}
-(void)fetchMyProjects {
    _ref = [[FIRDatabase database] reference];
    [[_ref child:@"keyboards"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Get user value
        if (snapshot.value != (id)[NSNull null]) {
            keyboardArray=snapshot.value;
            [keyboardArray removeObjectIdenticalTo:[NSNull null]];
        } else {
            keyboardArray=[[NSMutableArray alloc]init];
        }
    } withCancelBlock:^(NSError * _Nonnull error) {
    }];
}

-(void)update {

    _ref = [[FIRDatabase database] reference];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"projectid ==[c] %@",_projectIdStr];
    NSMutableArray *array = [[keyboardArray filteredArrayUsingPredicate:predicate] mutableCopy];
    [keyboardArray removeObjectsInArray:array];
    
    for (int i=0; i<array.count; i++) {
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        dict=[array objectAtIndex:i];
        
        if (_projectArray.count==0) {
            [dict setValue:@"" forKey:@"mojis"];
        } else {
            [dict setValue:_projectArray forKey:@"mojis"];
        }

        
            [keyboardArray addObject:dict];
    }
    
    
    [[_ref child:@"keyboards"]  setValue:keyboardArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
        if (error) {
            NSLog(@"%@",error.localizedDescription);
            
        } else {
            NSLog(@"success");
        }
    }];

}

- (IBAction)Back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _projectArray.count+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ProjectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProjectCell" forIndexPath:indexPath];
    
    cell.addImg.hidden=YES;
    NSMutableDictionary *dict;
    if (indexPath.row==_projectArray.count) {
        cell.mojiImg.image=[UIImage imageNamed:@"addimg"];
        cell.addImg.hidden=NO;
        cell.titleLbl.text=@"Add";
        cell.indicator.hidden=YES;
        [cell.indicator stopAnimating];
    } else {
        dict=[_projectArray objectAtIndex:indexPath.row];
        cell.indicator.hidden=NO;
        [cell.indicator startAnimating];
        cell.titleLbl.text=[NSString stringWithFormat:@"%@",[[_projectArray objectAtIndex:indexPath.row]valueForKey:@"name"]];
       
        [cell.mojiImg sd_setImageWithURL:[NSURL URLWithString:[[_projectArray objectAtIndex:indexPath.row]valueForKey:@"image"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            CGFloat boundsScale=cell.mojiImg.frame.size.width / cell.mojiImg.frame.size.height;
            CGFloat imageScale=image.size.width / image.size.height;
            CGRect drawingRect = cell.mojiImg.bounds;
            if (boundsScale > imageScale) {
                drawingRect.size.width =  drawingRect.size.height * imageScale;
                drawingRect.origin.x = (cell.mojiImg.frame.size.width - drawingRect.size.width) / 2;
            } else{
                drawingRect.size.height = drawingRect.size.width / imageScale;
                drawingRect.origin.y = (cell.mojiImg.frame.size.height - drawingRect.size.height) / 2;
            }
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:drawingRect cornerRadius:2];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.path = maskPath.CGPath;
            cell.mojiImg.layer.mask = maskLayer;
            cell.indicator.hidden=YES;
            [cell.indicator stopAnimating];

        }];

    }
    
    cell.backView.layer.cornerRadius=14;
    cell.backView.clipsToBounds=YES;
    
    cell.selectedView.layer.cornerRadius=8;
    cell.selectedView.clipsToBounds=YES;
    
    
    if ([indexPaths containsObject:dict]) {
        cell.selectedView.hidden=NO;
    } else {
        cell.selectedView.hidden=YES;
    }
    
    return cell;
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableArray *indexPathArray = [NSMutableArray arrayWithObject:indexPath];
    
    if (indexPath.row !=_projectArray.count) {
        if ([deleteStr isEqualToString:@"select"]) {
            NSMutableDictionary *dict=[_projectArray objectAtIndex:indexPath.row];
            if (indexPaths.count==0) {
                
                indexPaths = [NSMutableArray arrayWithObject:dict];
                
            } else {
                if ([indexPaths containsObject:dict]) {
                    [indexPaths removeObject:dict];
                } else {
                    [indexPaths addObject:dict];
                }
            }
            [collectionView reloadItemsAtIndexPaths:indexPathArray];
        } else {
//            SubCategories *sub=[[SubCategories alloc]initWithNibName:@"SubCategories" bundle:nil];
//            sub.projectArray=[[_projectArray valueForKey:@"mojis"] objectAtIndex:indexPath.row];
//            [self.navigationController pushViewController:sub animated:YES];
        }
    } else {
        
        if (![deleteStr isEqualToString:@"select"]) {
            [self pickImageMethod];
        }
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
        return CGSizeMake(100, 114);
    } else if ([[UIScreen mainScreen]bounds].size.height==736){
        return CGSizeMake(98, 112);
    } else {
        return CGSizeMake(112, 124);
    }
}
-(IBAction)PopupCross:(id)sender {
    popUpBackView.hidden=YES;
    [descriptionView resignFirstResponder];
}


-(void)pickImageMethod {
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [self checkCameraAuthorization];
    }];
    
    UIAlertAction *gallery = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [self checkPhotoAuthorization];
    }];
    
    UIAlertAction *video = [UIAlertAction actionWithTitle:@"Video" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
        imagePicker.videoMaximumDuration=3.0f;
        [self presentViewController:imagePicker animated:YES completion:NULL];
        isCamera=@"no";

    }];
    
    
    [alertController addAction:gallery];
    [alertController addAction:camera];
    [alertController addAction:video];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
    
}


-(void)checkPhotoAuthorization {
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if (status == PHAuthorizationStatusAuthorized) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        isCamera=@"no";
        
    }
    
    else if (status == PHAuthorizationStatusDenied) {
        
        
        [self accessMethod:@"Please go to Settings and enable the photos for this app to use this feature."];
        
    }
    
    else if (status == PHAuthorizationStatusNotDetermined) {
        
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:picker animated:YES completion:NULL];
                isCamera=@"no";
                
            }
            
            else {
                [self accessMethod:@"Please go to Settings and enable the photos for this app to use this feature."];
            }
        }];
    }
    
    else if (status == PHAuthorizationStatusRestricted) {
        // Restricted access - normally won't happen.
    }
}

-(void) checkCameraAuthorization {
    
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(status == AVAuthorizationStatusAuthorized) { // authorized
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie,(NSString *)kUTTypeImage, nil];
        picker.videoMaximumDuration=3.0f;
        [self presentViewController:picker animated:YES completion:NULL];
        isCamera=@"yes";
        
    }
    else if(status == AVAuthorizationStatusDenied){ // denied
        if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                // until iOS 8. So for iOS 7 permission will always be granted.
                
                NSLog(@"DENIED");
                
                if (granted) {
                    // Permission has been granted. Use dispatch_async for any UI updating
                    // code because this block may be executed in a thread.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                        picker.delegate = self;
                        picker.allowsEditing = YES;
                        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        picker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie,(NSString *)kUTTypeImage, nil];
                        picker.videoMaximumDuration=3.0f;
                        [self presentViewController:picker animated:YES completion:NULL];
                        isCamera=@"yes";
                        
                    });
                } else {
                    // Permission has been denied.
                    [self accessMethod:@"Please go to Settings and enable the camera for this app to use this feature."];
                    
                }
            }];
        }
    }
    else if(status == AVAuthorizationStatusRestricted){ // restricted
        [self accessMethod:@"Please go to Settings and enable the camera for this app to use this feature."];
        
    }
    else if(status == AVAuthorizationStatusNotDetermined){ // not determined
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){ // Access has been granted ..do something
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie,(NSString *)kUTTypeImage, nil];
                picker.videoMaximumDuration=3.0f;
                [self presentViewController:picker animated:YES completion:NULL];
                isCamera=@"yes";
                
            } else { // Access denied ..do something
                [self accessMethod:@"Please go to Settings and enable the camera for this app to use this feature."];
            }
        }];
    }
}

-(void)accessMethod:(NSString *)message {
    
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"Not Authorized" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction *Settings = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    
    [alertController addAction:Settings];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
    
}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    NSURL *assetURL = info[UIImagePickerControllerReferenceURL];
    NSString *extension = [assetURL pathExtension];
    CFStringRef imageUTI = (UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,(__bridge CFStringRef)extension , NULL));
    if (UTTypeConformsTo(imageUTI, kUTTypeGIF)) {
        // Handle PNG
        NSLog(@"gif");
    } 
    
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
        videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
        NSString *moviePath = [videoUrl path];
        NSLog(@"%@",moviePath);
        typeStr=@"gif";
        if (videoUrl){
            AVURLAsset *sourceAsset = [AVURLAsset URLAssetWithURL:videoUrl options:nil];
            CMTime duration = sourceAsset.duration;
            float seconds = CMTimeGetSeconds(duration);
            NSLog(@"duration: %.2f", seconds);
            [self performSelector:@selector(displayVideoEditorForVideo) withObject:nil afterDelay:0.2];
        }
    } else if (UTTypeConformsTo(imageUTI, kUTTypeGIF)) {
        
    
        PHAsset * asset = [[PHAsset fetchAssetsWithALAssetURLs:@[assetURL] options:nil] lastObject];
        if (asset) {
            PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
            options.synchronous = YES;
            options.networkAccessAllowed = NO;
            options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
            [[PHImageManager defaultManager] requestImageDataForAsset:asset options:options resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
                NSNumber * isError = [info objectForKey:PHImageErrorKey];
                NSNumber * isCloud = [info objectForKey:PHImageResultIsInCloudKey];
                gifData=imageData;
                typeStr=@"gifImg";

                if ([isError boolValue] || [isCloud boolValue] || ! imageData) {
                    // fail
                } else {
                    // success, data is in imageData
                }
            }];
        }

            popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
            [UIView animateWithDuration:0.3/1.5 animations:^{
                popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3/2 animations:^{
                    popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.3/2 animations:^{
                        popUpView.transform = CGAffineTransformIdentity;
                    }];
                }];
            }];
            
            descriptionView.text=@"";
            popUpBackView.hidden=NO;
            
        
            
        } else {
       
            typeStr=@"image";
            projectImg=info[UIImagePickerControllerOriginalImage];
            CGRect crop = [[info valueForKey:@"UIImagePickerControllerCropRect"] CGRectValue];
            projectImg = [self ordinaryCrop:projectImg toRect:crop];
            
            CGRect rect = CGRectMake(0,0,600,600);
            UIGraphicsBeginImageContext( rect.size );
            [projectImg drawInRect:rect];
            UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            NSData *imageData = UIImagePNGRepresentation(picture1);
            projectImg=[UIImage imageWithData:imageData];
            
            [self performSelector:@selector(displayAdobeImageEditor) withObject:nil afterDelay:0.2];

            
            
        
    }
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(UIImage *)ordinaryCrop:(UIImage *)imageToCrop toRect:(CGRect)cropRect
{
    CGAffineTransform rectTransform;
    switch (imageToCrop.imageOrientation)
    {
        case UIImageOrientationLeft:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(M_PI_2), 0, -imageToCrop.size.height);
            break;
        case UIImageOrientationRight:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(-M_PI_2), -imageToCrop.size.width, 0);
            break;
        case UIImageOrientationDown:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(-M_PI), -imageToCrop.size.width, -imageToCrop.size.height);
            break;
        default:
            rectTransform = CGAffineTransformIdentity;
    };
    rectTransform = CGAffineTransformScale(rectTransform, imageToCrop.scale, imageToCrop.scale);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], CGRectApplyAffineTransform(cropRect, rectTransform));
    UIImage *cropped = [UIImage imageWithCGImage:imageRef scale:0.1 orientation:imageToCrop.imageOrientation];
    CGImageRelease(imageRef);
    return cropped;
}

-(void)displayAdobeImageEditor {
    AdobeUXImageEditorViewController *editorController = [[AdobeUXImageEditorViewController alloc] initWithImage:projectImg];
    [editorController setDelegate:self];
    [self presentViewController:editorController animated:YES completion:nil];
}
- (void)photoEditor:(AdobeUXImageEditorViewController *)editor finishedWithImage:(UIImage *)image
{
    projectImg=image;
    popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView animateWithDuration:0.3/1.5 animations:^{
        popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                popUpView.transform = CGAffineTransformIdentity;
            }];
        }];
    }];
    
    descriptionView.text=@"";
    popUpBackView.hidden=NO;
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)photoEditorCanceled:(AdobeUXImageEditorViewController *)editor
{
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)displayVideoEditorForVideo
{
    VideoEditorVC *obj = [[UIStoryboard storyboardWithName:@"Storyboard_VE" bundle:[NSBundle bundleWithIdentifier:@"com.VideoEditor"]] instantiateViewControllerWithIdentifier:@"VideoEditorVC"];
    obj.delegate = self;
    obj.videoPath = videoUrl;
    obj.clientKey = @"58b15d8b07d8e";
    obj.clientSecretKey = @"VES58b15d8b078491.15426123";
    
    // obj.excludedFunctionalities = @[VEFilter]; // User can exclude multiple functionalities while using SDK. Currently we excluded Filter feature.
    [self presentViewController:obj animated:YES completion:nil];
}

#pragma mark - VideoEditorSDK Delegate Method
-(void)videoEditorSDKFinishedWithUrl:(NSURL *)url{
    NSLog(@"Video editing finished url : %@",url);
    
    [indicator startAnimating];
    indicator.hidden=NO;
    self.view.userInteractionEnabled=false;
    
    NSGIFRequest * request = [NSGIFRequest requestWithSourceVideo:url];
    request.progressHandler = ^(double progress, NSUInteger position, NSUInteger length, CMTime time, BOOL *stop, NSDictionary *frameProperties) {
        NSLog(@"%f - %lu - %lu - %lld - %@", progress, (unsigned long)position, (unsigned long)length, time.value, frameProperties);
    };
    
    [NSGIF create:request completion:^(NSURL * GifURL) {
        NSLog(@"Finished generating GIF: %@", GifURL);
        gifPath = [[GifURL absoluteString] stringByReplacingOccurrencesOfString:@"file:///" withString:@""];
        
        
        
        
        indicator.hidden=YES;
        [indicator stopAnimating];
        self.view.userInteractionEnabled=true;

        if (GifURL) {
            NSLog(@"success");
            popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
            [UIView animateWithDuration:0.3/1.5 animations:^{
                popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3/2 animations:^{
                    popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.3/2 animations:^{
                        popUpView.transform = CGAffineTransformIdentity;
                    }];
                }];
            }];
            
            descriptionView.text=@"";
            popUpBackView.hidden=NO;
          //  [descriptionView becomeFirstResponder];
        } else {
            NSLog(@"not success");
            UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"There is an error while creating gif image.Please don't trim the video less than 1 second." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];

        }
        
    }];

}

-(void)videoEditorSDKCanceled{
    NSLog(@"Video editing cancel");
}


-(IBAction)Upload:(id)sender {
    if (descriptionView.text.length>0) {
        [descriptionView resignFirstResponder];
        [self uploadMethod];
    } else {
        UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Enter the title !" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}
-(void)uploadMethod {
    [indicator startAnimating];
    indicator.hidden=NO;
    _ref = [[FIRDatabase database] reference];
    FIRDatabaseReference * autoId = [_ref childByAutoId];
    NSString *uniqueId =autoId.key;
    
    FIRStorage *storage = [FIRStorage storage];
    _storageRef = [storage referenceForURL:@"gs://api-project-405149633111.appspot.com"];
    NSString *imageID = [[NSUUID UUID] UUIDString];
    NSString *imageName;
    
    if ([typeStr isEqualToString:@"gif"] || [typeStr isEqualToString:@"gifImg"]) {
        imageName = [NSString stringWithFormat:@"MojisPictures/%@.gif",imageID];
    } else {
        imageName = [NSString stringWithFormat:@"MojisPictures/%@.jpg",imageID];
    }
    
    FIRStorageReference *profilePicRef = [_storageRef child:imageName];
    FIRStorageMetadata *metadata = [[FIRStorageMetadata alloc] init];
    NSData *imageData;
    if ([typeStr isEqualToString:@"gif"]) {
        metadata.contentType = @"image/gif";
        imageData = [NSData dataWithContentsOfFile:gifPath];

    } else if ([typeStr isEqualToString:@"gifImg"]) {
        metadata.contentType = @"image/gif";
        imageData=gifData;
    } else {
        metadata.contentType = @"image/jpeg";
        
        if ([isCamera isEqualToString:@"yes"]) {
            imageData = UIImageJPEGRepresentation(projectImg, 0.0);
        } else {
            imageData = UIImagePNGRepresentation(projectImg);
        }
    }
  
    deleteBtn.enabled=NO;
    UploadBtn.enabled=NO;

    [profilePicRef putData:imageData metadata:metadata completion:^(FIRStorageMetadata *metadata, NSError *error) {
        if (!error) {
            NSString *profileImageURL = metadata.downloadURL.absoluteString;
            
            
            NSMutableArray *testArray=[[NSMutableArray alloc]initWithObjects:@{@"name": descriptionView.text, @"image":profileImageURL, @"type":typeStr, @"mojiid":uniqueId}, nil];
            
            [_projectArray addObjectsFromArray:testArray];
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:_projectArray forKey:@"mojis"];
            
            _ref = [[FIRDatabase database] reference];
            [[[_ref child:@"projects"] child:_keyStr] updateChildValues:dict withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                if (error) {
                    NSLog(@"%@",error.localizedDescription);
                    [self alertView:@"" message:error.localizedDescription];
                }else {
                    popUpBackView.hidden=YES;
                    [projectCollec reloadData];
                  //  [self alertView:@"" message:@"Moji has been successfully uploaded !"];
                    [self update];

                }
                
                [indicator stopAnimating];
                indicator.hidden=YES;
                deleteBtn.enabled=YES;
                UploadBtn.enabled=YES;

            }];
        }
        else if (error) {
            [self alertView:@"" message:error.localizedDescription];
            
            NSLog(@"Failed to Register User with profile image");
            [indicator stopAnimating];
            indicator.hidden=YES;
            deleteBtn.enabled=YES;
            UploadBtn.enabled=YES;

        }
    }];
}
-(void) alertView:(NSString *)title message:(NSString *)messageStr {
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:title message:messageStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
}


- (IBAction)Share:(id)sender {
    
    NSString *shareStr=[NSString stringWithFormat:@"Moji App: Share stickers and moji with friends."];
    NSArray *items = @[shareStr];
    
    // build an activity view controller
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    
    // and present it
    [self presentViewController:controller animated:YES completion:^{
        // executes after the user selects something
    }];
    
}

- (IBAction)Delete:(id)sender {
    if ([sender tag]==0) {
        if (_projectArray.count>0) {
            deleteStr=@"select";
            deleteView.hidden=NO;
        }

    } else {
        if (indexPaths.count>0) {
            
            UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to delete ?" preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            
            UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                deleteView.hidden=YES;
                deleteStr=@"";
                
                NSMutableArray *tempArray=[[NSMutableArray alloc]init];
                tempArray=[indexPaths mutableCopy];
                
                
                [_projectArray removeObjectsInArray:tempArray];
                indexPaths=[[NSMutableArray alloc]init];
                [projectCollec reloadData];
                
                
                for (int i=0; i<tempArray.count; i++) {
                    
                    NSString *removeStr=[[tempArray valueForKey:@"image"] objectAtIndex:i];
                    NSRange range = [removeStr rangeOfString:@"MojisPictures"];
                    NSRange range1 = [removeStr rangeOfString:@"?alt"];
                    
                    if (range.location != NSNotFound) {
                        removeStr = [removeStr substringWithRange:NSMakeRange(range.location, range1.location-range.location)];
                        removeStr=[removeStr stringByReplacingOccurrencesOfString:@"%2F" withString:@"/"];
                        NSLog(@"%@",removeStr);
                        
                        
                        FIRStorage *storage = [FIRStorage storage];
                        _storageRef = [storage referenceForURL:@"gs://api-project-405149633111.appspot.com"];
                        
                        FIRStorageReference *desertRef = [_storageRef child:removeStr];
                        
                        // Delete the file
                        [desertRef deleteWithCompletion:^(NSError *error){
                            if (error != nil) {
                                NSLog(@"%@",error.localizedDescription) ;
                            } else {
                                NSLog(@"File deleted successfully") ;
                            }
                        }];
                        
                        
                        
                    } else {
                        NSLog(@"= is not found");
                    }
                    
                }
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                
                if (_projectArray.count==0) {
                    [dict setValue:@"" forKey:@"mojis"];
                } else {
                    [dict setValue:_projectArray forKey:@"mojis"];
                }
                
                
                _ref = [[FIRDatabase database] reference];
                [[[_ref child:@"projects"] child:_keyStr] updateChildValues:dict withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                    if (error) {
                        NSLog(@"%@",error.localizedDescription);
                        [self alertView:@"" message:error.localizedDescription];
                    }else {
                        popUpBackView.hidden=YES;
                        [projectCollec reloadData];
                      //  [self alertView:@"" message:@"Moji has been successfully deleted !"];
                        [self update];

                    }
                }];
                
                
            }];
            
            [alertController addAction:delete];
            [alertController addAction:cancel];
            [self presentViewController:alertController animated:YES completion:nil];
            
        } else {
            [self alertView:@"" message:@"Please select atleast one moji to delete !"];
        }

    }
}


- (IBAction)Cancel:(id)sender {
    deleteView.hidden=YES;
    deleteStr=@"";
    indexPaths=[[NSMutableArray alloc]init];
    [projectCollec reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)sender {
    [self.keyboardControls setActiveField:sender];
    [self setViewMovedUp12:YES textfieldd:sender];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self setViewMovedUp12:NO textfieldd:textField];
}
#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction {
    UIView *view;
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        view = field.superview.superview;
    } else {
        view = field.superview.superview.superview;
    }
    
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

-(void)setViewMovedUp12:(BOOL)movedUp textfieldd:(UITextField *)textfield {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    CGRect rect = self.view.frame;
    if (movedUp) {
        if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
            rect.origin.y=-105;
        } else if ([[UIScreen mainScreen]bounds].size.height==667) {
            rect.origin.y=-30;
        }
    }
    else {
        rect.origin.y=0;
    }
    
    self.view.frame = CGRectMake(0, rect.origin.y, rect.size.width, rect.size.height);
    
    [UIView commitAnimations];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    [descriptionView resignFirstResponder];
}



@end
