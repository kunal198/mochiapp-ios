//
//  Shop.h
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"
@import FirebaseDatabase;

@interface Upload : UIViewController<BSKeyboardControlsDelegate>
{
    Singleton *singletonn;
    IBOutlet UITableView *shopTbl;
    NSMutableArray *projectArray,*keyArray,*uploadArray,*uploadedArray,*reportArray;
    IBOutlet UIView *backView;
    IBOutlet UIButton *uploadBtn,*uploadedBtn;
    IBOutlet UILabel *uploadlbl,*uploadedLbl;
    NSString *statusStr,*priceStr;
    
    IBOutlet UIView *popUpView,*popUpBackView;
    IBOutlet UITextView *descriptionView;
    IBOutlet UITextField *priceTxt;
    IBOutlet UIButton *UploadBtn,*deleteBtn;
    IBOutlet UIImageView *freeImg,*paidImg;
    IBOutlet UILabel *priceLbl,*alertLbl;
    
    IBOutlet UIActivityIndicatorView *indicator;
    NSInteger index;
    NSMutableArray *keyboardArray;
    
    NSString *userId;
}

@property (strong, nonatomic) FIRDatabaseReference *ref;

@end
