//
//  Shop.m
//  Moji
//
//  Created by Brst-Pc109 on 10/02/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

#import "Upload.h"
#import "ShopCell.h"
#import "ShopMojiCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface Upload ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

@end

@implementation Upload

- (void)viewDidLoad {
    [super viewDidLoad];
    singletonn=[Singleton sharedInstance];
    
    if ([singletonn.isLoginstr isEqualToString:@"yes"]) {
        userId=singletonn.userId;
    } else {
        userId=singletonn.uniqueId;
    }

    
    statusStr=@"uploaded";
    
    
    backView.layer.cornerRadius=18;
    backView.layer.borderWidth=1;
    backView.layer.borderColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1].CGColor;
    backView.clipsToBounds=YES;
    
    
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, priceTxt.frame.size.height)];
    priceTxt.leftView = paddingView;
    priceTxt.leftViewMode = UITextFieldViewModeAlways;
    
    popUpView.layer.cornerRadius=10;
    popUpView.clipsToBounds=YES;
    
    UploadBtn.layer.cornerRadius=18;
    UploadBtn.clipsToBounds=YES;
    popUpBackView.hidden=YES;
    
    
    UIColor *color = [UIColor lightGrayColor];
    color = [color colorWithAlphaComponent:0.4f];
    
    descriptionView.layer.cornerRadius=6;
    descriptionView.layer.borderWidth=1.4;
    descriptionView.layer.borderColor=color.CGColor;
    descriptionView.clipsToBounds=YES;
    
    
    priceTxt.layer.cornerRadius=6;
    priceTxt.layer.borderWidth=1.4;
    priceTxt.layer.borderColor=color.CGColor;
    priceTxt.clipsToBounds=YES;
    
    
    NSArray *fields = @[ descriptionView];
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    
    priceStr=@"free";
    [priceTxt setEnabled:NO];
    
    [indicator startAnimating];
    indicator.hidden=NO;
    [self fetchProjects];
    [self fetchMyProjects];
    [self reportProjects];
}

-(void)reportProjects {
    _ref = [[FIRDatabase database] reference];
    [[_ref child:@"reports"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Get user value
        if (snapshot.value != (id)[NSNull null]) {
            reportArray=snapshot.value;
            [reportArray removeObjectIdenticalTo:[NSNull null]];
        } else {
            reportArray=[[NSMutableArray alloc]init];
        }
        
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        
    }];
}

-(void)fetchMyProjects {
    _ref = [[FIRDatabase database] reference];
    [[_ref child:@"keyboards"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Get user value
        if (snapshot.value != (id)[NSNull null]) {
            keyboardArray=snapshot.value;
            [keyboardArray removeObjectIdenticalTo:[NSNull null]];
        } else {
            keyboardArray=[[NSMutableArray alloc]init];
        }
    } withCancelBlock:^(NSError * _Nonnull error) {
    }];
}


-(void)fetchProjects {
    _ref = [[FIRDatabase database] reference];
    [[_ref child:@"projects"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Get user value
        if (snapshot.value != (id)[NSNull null]) {
            keyArray=snapshot.value;
            projectArray=snapshot.value;
            [projectArray removeObjectIdenticalTo:[NSNull null]];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userid contains[cd] %@",userId];
            projectArray = [[projectArray filteredArrayUsingPredicate:predicate] mutableCopy];
            
            
            
            NSMutableArray *tempArray=[[NSMutableArray alloc]init];
            tempArray=[projectArray mutableCopy];
            for (int i=0; i<projectArray.count; i++) {
                
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[projectArray objectAtIndex:i];
                NSArray *mojisArray=[[projectArray valueForKey:@"mojis"] objectAtIndex:i];
                
                if ([mojisArray isKindOfClass:[NSString class]]) {
                    [tempArray removeObject:dict];
                }
            }
            
            projectArray=tempArray;
            uploadArray=tempArray;
            
            
            NSPredicate *predicates = [NSPredicate predicateWithFormat:@"status ==[c] %@",@"1"];
            uploadedArray = [[projectArray filteredArrayUsingPredicate:predicates] mutableCopy];
            
            
        } else {
            projectArray=[[NSMutableArray alloc]init];
            keyArray=[[NSMutableArray alloc]init];
        }
        
        
        if ([statusStr isEqualToString:@"uploaded"]) {
            alertLbl.text=@"There is no mojis in uploaded section !";
            if (uploadedArray.count==0) {
                alertLbl.hidden=NO;
            } else {
                alertLbl.hidden=YES;
            }
            
        } else if ([statusStr isEqualToString:@"upload"]) {
            alertLbl.text=@"There is no mojis in upload section !";
            if (uploadArray.count==0) {
                alertLbl.hidden=NO;
            } else {
                alertLbl.hidden=YES;
            }
            
        }
        
        
        [shopTbl reloadData];
        indicator.hidden=YES;
        [indicator stopAnimating];
        
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        indicator.hidden=YES;
        [indicator stopAnimating];
        
    }];
    
    
}



-(IBAction)Back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)Uploaded:(id)sender {
    statusStr=@"uploaded";
    uploadedBtn.backgroundColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    uploadedLbl.textColor=[UIColor whiteColor];
    uploadBtn.backgroundColor=[UIColor clearColor];
    uploadlbl.textColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    [shopTbl reloadData];
    
    
    alertLbl.text=@"There is no mojis in uploaded section !";
    if (uploadedArray.count==0) {
        alertLbl.hidden=NO;
    } else {
        alertLbl.hidden=YES;
    }
    
    
    
    
}
-(IBAction)Upload:(id)sender {
    statusStr=@"upload";
    uploadedBtn.backgroundColor=[UIColor clearColor];
    uploadedLbl.textColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    uploadBtn.backgroundColor=[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1];
    uploadlbl.textColor=[UIColor whiteColor];
    [shopTbl reloadData];
    alertLbl.text=@"There is no mojis in upload section !";
    if (uploadArray.count==0) {
        alertLbl.hidden=NO;
    } else {
        alertLbl.hidden=YES;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([statusStr isEqualToString:@"uploaded"]) {
        return uploadedArray.count;
        
    }
    return uploadArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 111;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ShopCell *cell = (ShopCell *)[tableView dequeueReusableCellWithIdentifier:@"ShopCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShopCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.btn.layer.cornerRadius=13;
    cell.btn.clipsToBounds=YES;
    cell.addtoKeyboardLbl.hidden=YES;
    cell.deleteBtn.hidden=YES;
    cell.backView.hidden=NO;
    [cell.btn setBackgroundColor:[UIColor colorWithRed:148.0f/255 green:218.0f/255 blue:80.0f/255 alpha:1]];
    cell.btn.tag=indexPath.row;
    
    [cell.flagBtn addTarget:self action:@selector(Flag:) forControlEvents:UIControlEventTouchUpInside];
    cell.flagBtn.tag=indexPath.row;

    
    if ([statusStr isEqualToString:@"uploaded"]) {
        
        cell.nameLbl.text=[[uploadedArray objectAtIndex:indexPath.row] valueForKey:@"name"];
        cell.downloadLbl.text=[NSString stringWithFormat:@"%@",[[uploadedArray objectAtIndex:indexPath.row] valueForKey:@"download"]];
        cell.flagLbl.text=[NSString stringWithFormat:@"%@",[[uploadedArray objectAtIndex:indexPath.row] valueForKey:@"flag"]];
        
        
        
        [cell.btn setTitle:@"Remove" forState:UIControlStateNormal];
        [cell.btn addTarget:self action:@selector(UploadedBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [cell.downloadBtn addTarget:self action:@selector(Download:) forControlEvents:UIControlEventTouchUpInside];
        cell.downloadBtn.tag=indexPath.row;
        
        
    } else if ([statusStr isEqualToString:@"upload"]) {
        
        cell.nameLbl.text=[[uploadArray objectAtIndex:indexPath.row] valueForKey:@"name"];
        cell.downloadLbl.text=[NSString stringWithFormat:@"%@",[[uploadArray objectAtIndex:indexPath.row] valueForKey:@"download"]];
        cell.flagLbl.text=[NSString stringWithFormat:@"%@",[[uploadArray objectAtIndex:indexPath.row] valueForKey:@"flag"]];
        
        
        cell.btn.tag=indexPath.row;
        [cell.btn addTarget:self action:@selector(UploadedBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([[[uploadArray valueForKey:@"status"] objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            [cell.btn setTitle:@"Upload" forState:UIControlStateNormal];
            [cell.btn setBackgroundColor:[UIColor colorWithRed:87.0f/255 green:195.0f/255 blue:254.0f/255 alpha:1]];
        } else {
            [cell.btn setTitle:@"Uploaded" forState:UIControlStateNormal];
        }
        
        [cell.downloadBtn addTarget:self action:@selector(Download:) forControlEvents:UIControlEventTouchUpInside];
        cell.downloadBtn.tag=indexPath.row;

    }
    
    cell.mojiCollec.delegate=self;
    cell.mojiCollec.dataSource=self;
    cell.mojiCollec.tag=indexPath.row;
    [cell.mojiCollec registerNib:[UINib nibWithNibName:@"ShopMojiCell" bundle:nil] forCellWithReuseIdentifier:@"ShopMojiCell"];
    [cell.mojiCollec reloadData];
    return cell;
}
-(IBAction)Flag:(UIButton *)sender {
    if ([statusStr isEqualToString:@"uploaded"]){
        
        if ([[[uploadedArray valueForKey:@"userid"] objectAtIndex:[sender tag]] isEqualToString:userId]) {
            [self alertView:@"" message:@"You can't report your own moji."];
        } else {
            

        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"userid ==[c] %@",userId];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"projectid ==[c] %@",[[uploadedArray valueForKey:@"projectid"] objectAtIndex:[sender tag]]];
        
        
        NSCompoundPredicate *predicate=predicate=[NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1,predicate2]];
        NSMutableArray *array = [[reportArray filteredArrayUsingPredicate:predicate] mutableCopy];
        if (array.count>0) {
            [self alertView:@"" message:@"You are already reported to this mojis."];
            
        } else {
            
            UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to report?" preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            
            UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Report" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {

            NSDictionary *dict1=[uploadedArray objectAtIndex:[sender tag]];
            NSInteger anIndex=[keyArray indexOfObject:dict1];
            if(NSNotFound == anIndex) {
                NSLog(@"not found");
            }
            
            
            NSInteger fVal=[[dict1 objectForKey:@"flag"] integerValue];
            fVal+=1;
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:[NSString stringWithFormat:@"%ld",(long)fVal] forKey:@"flag"];
            
            [indicator startAnimating];
            indicator.hidden=NO;
            
            
            _ref = [[FIRDatabase database] reference];
            [[[_ref child:@"projects"] child:[NSString stringWithFormat:@"%ld",(long)anIndex]] updateChildValues:dict withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                if (error) {
                    NSLog(@"%@",error.localizedDescription);
                    [self alertView:@"" message:error.localizedDescription];
                }else {
                    
                    
                    NSMutableDictionary *reportDict=[[NSMutableDictionary alloc]init];
                    [reportDict setValue:userId forKey:@"userid"];
                    [reportDict setValue:[dict1 objectForKey:@"projectid"] forKey:@"projectid"];
                    [reportArray addObject:reportDict];
                    
                    [[_ref child:@"reports"]  setValue:reportArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                        if (error) {
                            NSLog(@"%@",error.localizedDescription);
                            [self alertView:@"" message:error.localizedDescription];
                        } else {
                            
                           // [self alertView:@"" message:@"You have been successfully reported to this mojis."];
                            
                        }
                        [indicator stopAnimating];
                        indicator.hidden=YES;
                        
                    }];
                    

                    
                    [shopTbl reloadData];
                    
                }
                [indicator stopAnimating];
                indicator.hidden=YES;
                [shopTbl reloadData];
            }];
            }];
            
            [alertController addAction:delete];
            [alertController addAction:cancel];
            [self presentViewController:alertController animated:YES completion:nil];
            

        }
        
        }
        
    } else if ([[[uploadArray valueForKey:@"status"] objectAtIndex:[sender tag]] isEqualToString:@"1"] ) {
        
        if ([[[uploadArray valueForKey:@"userid"] objectAtIndex:[sender tag]] isEqualToString:userId]) {
            [self alertView:@"" message:@"You can't report your own moji."];
        } else {
            

        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"userid ==[c] %@",userId];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"projectid ==[c] %@",[[uploadArray valueForKey:@"projectid"] objectAtIndex:[sender tag]]];
        
        
        NSCompoundPredicate *predicate=predicate=[NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1,predicate2]];
        NSMutableArray *array = [[reportArray filteredArrayUsingPredicate:predicate] mutableCopy];
        if (array.count>0) {
            [self alertView:@"" message:@"You are already reported to this mojis."];
            
        } else {
            
            UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to report?" preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            
            UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Report" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {

            
            NSDictionary *dict1=[uploadArray objectAtIndex:[sender tag]];
            NSInteger anIndex=[keyArray indexOfObject:dict1];
            if(NSNotFound == anIndex) {
                NSLog(@"not found");
            }
            
            
            NSInteger fVal=[[dict1 objectForKey:@"flag"] integerValue];
            fVal+=1;
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:[NSString stringWithFormat:@"%ld",(long)fVal] forKey:@"flag"];
            
            [indicator startAnimating];
            indicator.hidden=NO;
            
            
            _ref = [[FIRDatabase database] reference];
            [[[_ref child:@"projects"] child:[NSString stringWithFormat:@"%ld",(long)anIndex]] updateChildValues:dict withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                if (error) {
                    NSLog(@"%@",error.localizedDescription);
                    [self alertView:@"" message:error.localizedDescription];
                }else {
                    
                    
                    NSMutableDictionary *reportDict=[[NSMutableDictionary alloc]init];
                    [reportDict setValue:userId forKey:@"userid"];
                    [reportDict setValue:[dict1 objectForKey:@"projectid"] forKey:@"projectid"];
                    [reportArray addObject:reportDict];
                    
                    [[_ref child:@"reports"]  setValue:reportArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                        if (error) {
                            NSLog(@"%@",error.localizedDescription);
                            [self alertView:@"" message:error.localizedDescription];
                        } else {
                            
                          //  [self alertView:@"" message:@"You have been successfully reported to this mojis."];
                            
                        }
                        [indicator stopAnimating];
                        indicator.hidden=YES;
                        
                    }];
                    

                    
                    [shopTbl reloadData];
                    
                }
                [indicator stopAnimating];
                indicator.hidden=YES;
                [shopTbl reloadData];
            }];

            }];
            
            [alertController addAction:delete];
            [alertController addAction:cancel];
            [self presentViewController:alertController animated:YES completion:nil];
            

        }
        
        
    }
    } else if ([[[uploadArray valueForKey:@"status"] objectAtIndex:[sender tag]] isEqualToString:@"0"]) {
        [self alertView:@"" message:@"Please upload the project to report mojis !"];
    }
}

-(IBAction)UploadedBtn:(UIButton *)sender {
    index=[sender tag];
    
    if ([statusStr isEqualToString:@"uploaded"]) {
        UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to remove ?" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        
        UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Remove" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            
            
            
            NSDictionary *dict1=[uploadedArray objectAtIndex:index];
            NSInteger anIndex=[keyArray indexOfObject:dict1];
            if(NSNotFound == anIndex) {
                NSLog(@"not found");
            }
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:@"0" forKey:@"status"];
            [dict setValue:@"" forKey:@"description"];
            
            [indicator startAnimating];
            indicator.hidden=NO;
            
            
            _ref = [[FIRDatabase database] reference];
            [[[_ref child:@"projects"] child:[NSString stringWithFormat:@"%ld",(long)anIndex]] updateChildValues:dict withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                if (error) {
                    NSLog(@"%@",error.localizedDescription);
                    [self alertView:@"" message:error.localizedDescription];
                }else {
                    popUpBackView.hidden=YES;
                    [shopTbl reloadData];
                  //  [self alertView:@"" message:@"Moji has been successfully removed !"];
                    
                    _ref = [[FIRDatabase database] reference];
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"projectid ==[c] %@",[dict1 valueForKey:@"projectid"]];
                    NSMutableArray *array = [[keyboardArray filteredArrayUsingPredicate:predicate] mutableCopy];
                    [keyboardArray removeObjectsInArray:array];
                    [[_ref child:@"keyboards"]  setValue:keyboardArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                        if (error) {
                            NSLog(@"%@",error.localizedDescription);
                            
                        } else {
                            NSLog(@"success");
                        }
                    }];
                }
                [descriptionView resignFirstResponder];
                popUpBackView.hidden=YES;
                [indicator stopAnimating];
                indicator.hidden=YES;
                [shopTbl reloadData];
                
            }];
            
            
            
            
            
            
        }];
        
        [alertController addAction:delete];
        [alertController addAction:cancel];
        [self presentViewController:alertController animated:YES completion:nil];
    } else if ([statusStr isEqualToString:@"upload"]) {
        
        descriptionView.text=@"";
        priceTxt.text=@"";
        if ([priceStr isEqualToString:@"free"]) {
            priceTxt.text=@"0.00";
        }
        
        
        
        if ([[[uploadArray valueForKey:@"status"] objectAtIndex:index] isEqualToString:@"0"]) {
            popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
            [UIView animateWithDuration:0.3/1.5 animations:^{
                popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3/2 animations:^{
                    popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.3/2 animations:^{
                        popUpView.transform = CGAffineTransformIdentity;
                    }];
                }];
            }];
            popUpBackView.hidden=NO;
            
            
            
            
        }
    }
}

-(IBAction)Uploading:(id)sender {
    
 //   if (descriptionView.text.length>0) {
        if (priceTxt.text.length>0) {
            
            NSDictionary *dict1=[uploadArray objectAtIndex:index];
            NSInteger anIndex=[keyArray indexOfObject:dict1];
            if(NSNotFound == anIndex) {
                NSLog(@"not found");
            }
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:@"1" forKey:@"status"];
            [dict setValue:descriptionView.text forKey:@"description"];
            
            [indicator startAnimating];
            indicator.hidden=NO;
            
            deleteBtn.enabled=NO;
            UploadBtn.enabled=NO;
            _ref = [[FIRDatabase database] reference];
            [[[_ref child:@"projects"] child:[NSString stringWithFormat:@"%ld",(long)anIndex]] updateChildValues:dict withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                if (error) {
                    NSLog(@"%@",error.localizedDescription);
                    [self alertView:@"" message:error.localizedDescription];
                }else {
                    popUpBackView.hidden=YES;
                    [shopTbl reloadData];
                   // [self alertView:@"" message:@"Moji has been successfully uploaded !"];
                    
                }
                [descriptionView resignFirstResponder];
                popUpBackView.hidden=YES;
                [indicator stopAnimating];
                indicator.hidden=YES;
                [shopTbl reloadData];
                
                deleteBtn.enabled=YES;
                UploadBtn.enabled=YES;

            }];
            
            
        }else {
            UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Enter the price !" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:cancel];
            [self presentViewController:alertController animated:YES completion:nil];
            
        }
        
/*    } else {
        UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Enter the description !" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:cancel];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
    */
}


-(IBAction)Download:(UIButton *)sender {
    
    NSMutableArray *array;
    NSDictionary *dict1;
    if ([statusStr isEqualToString:@"uploaded"]) {
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"userid ==[c] %@",[[uploadedArray valueForKey:@"userid"] objectAtIndex:[sender tag]]];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"name ==[c] %@",[[uploadedArray valueForKey:@"name"] objectAtIndex:[sender tag]]];
        NSCompoundPredicate *predicate=predicate=[NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1,predicate2]];
         array = [[keyboardArray filteredArrayUsingPredicate:predicate] mutableCopy];
        
         dict1=[uploadedArray objectAtIndex:[sender tag]];

    } else if ([statusStr isEqualToString:@"upload"]) {
        if ([[[uploadArray valueForKey:@"status"] objectAtIndex:[sender tag]] isEqualToString:@"0"]) {
             [self alertView:@"" message:@"Please upload the project to download mojis !"];
        } else {
            
            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"userid ==[c] %@",[[uploadArray valueForKey:@"userid"] objectAtIndex:[sender tag]]];
            NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"name ==[c] %@",[[uploadArray valueForKey:@"name"] objectAtIndex:[sender tag]]];
            NSCompoundPredicate *predicate=predicate=[NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1,predicate2]];
            array = [[keyboardArray filteredArrayUsingPredicate:predicate] mutableCopy];
            
             dict1=[uploadArray objectAtIndex:[sender tag]];
            

        }
    }
    if (![[[uploadArray valueForKey:@"status"] objectAtIndex:[sender tag]] isEqualToString:@"0"] || [statusStr isEqualToString:@"uploaded"]) {
    
    
   
    if (array.count>0) {
        [self alertView:@"" message:@"Moji is already downloaded to \"My Mojis\" in Shop section."];
        
    } else {
        
        
        NSInteger anIndex=[keyArray indexOfObject:dict1];
        if(NSNotFound == anIndex) {
            NSLog(@"not found");
        }
        
        
        NSInteger dVal=[[dict1 objectForKey:@"download"] integerValue];
        NSInteger fVal=[[dict1 objectForKey:@"flag"] integerValue];
        dVal+=1;
              
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:[NSString stringWithFormat:@"%ld",(long)dVal] forKey:@"download"];
        [dict setValue:[NSString stringWithFormat:@"%ld",(long)fVal] forKey:@"flag"];
        
        [indicator startAnimating];
        indicator.hidden=NO;
        
        
        self.view.userInteractionEnabled=false;
        _ref = [[FIRDatabase database] reference];
        [[[_ref child:@"projects"] child:[NSString stringWithFormat:@"%ld",(long)anIndex]] updateChildValues:dict withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
            if (error) {
                NSLog(@"%@",error.localizedDescription);
                [self alertView:@"" message:error.localizedDescription];
            }else {
                
                
                NSMutableDictionary *addDict1=[NSMutableDictionary dictionaryWithObjectsAndKeys:@"free", @"moji_type",
                                               @"0", @"moji_status",userId, @"moji_userid",
                                               nil] ;
                
                
                [addDict1 addEntriesFromDictionary:dict1];
                [keyboardArray addObject:addDict1];
                
                [[_ref child:@"keyboards"] setValue:keyboardArray withCompletionBlock:^( NSError *error,FIRDatabaseReference *ref) {
                    if (error) {
                        NSLog(@"%@",error.localizedDescription);
                        [self alertView:@"" message:error.localizedDescription];
                    } else {
                        
                        [self alertView:@"" message:@"Moji has been successfully downloaded to \"My Mojis\" in Shop section."];
                        
                    }
                    [indicator stopAnimating];
                    indicator.hidden=YES;
                    self.view.userInteractionEnabled=true;

                }];
                
                
                [shopTbl reloadData];
                
            }
            [indicator stopAnimating];
            indicator.hidden=YES;
            [shopTbl reloadData];
        }];
    }
    }
}


-(IBAction)PopupCross:(id)sender {
    popUpBackView.hidden=YES;
    [descriptionView resignFirstResponder];
    [priceTxt resignFirstResponder];
}
-(void) alertView:(NSString *)title message:(NSString *)messageStr {
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:title message:messageStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(IBAction)Price:(id)sender {
    if ([sender tag]==0) {
        freeImg.image=[UIImage imageNamed:@"radio_sel"];
        paidImg.image=[UIImage imageNamed:@"radio_unsel"];
        priceStr=@"free";
        [priceTxt setEnabled:NO];
        priceTxt.text=@"0.00";
        priceTxt.textColor=[UIColor lightGrayColor];
        priceLbl.textColor=[UIColor lightGrayColor];
        
    } else {
        freeImg.image=[UIImage imageNamed:@"radio_unsel"];
        paidImg.image=[UIImage imageNamed:@"radio_sel"];
        priceStr=@"paid";
        
        [priceTxt setEnabled:YES];
        priceTxt.text=@"";
        priceTxt.textColor=[UIColor blackColor];
        priceLbl.textColor=[UIColor blackColor];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSArray *mojisArray;
    if ([statusStr isEqualToString:@"uploaded"]) {
        mojisArray=[[uploadedArray valueForKey:@"mojis"] objectAtIndex:collectionView.tag];
    } else {
        mojisArray=[[uploadArray valueForKey:@"mojis"] objectAtIndex:collectionView.tag];
    }
    if ([mojisArray isKindOfClass:[NSString class]]) {
        mojisArray=[[NSMutableArray alloc]init];
    }
    return mojisArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ShopMojiCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ShopMojiCell" forIndexPath:indexPath];
    NSArray *mojisArray;
    if ([statusStr isEqualToString:@"uploaded"]) {
        mojisArray=[[uploadedArray valueForKey:@"mojis"] objectAtIndex:collectionView.tag];
    } else {
        mojisArray=[[uploadArray valueForKey:@"mojis"] objectAtIndex:collectionView.tag];
    }
    [cell.mojiImg sd_setImageWithURL:[NSURL URLWithString:[[mojisArray objectAtIndex:indexPath.row]valueForKey:@"image"]]];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[UIScreen mainScreen]bounds].size.height==736) {
        return CGSizeMake(56, 56);
    } else {
        return CGSizeMake(50, 50);
    }
}




#pragma mark -
#pragma mark Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)sender {
    [self.keyboardControls setActiveField:sender];
    [self setViewMovedUp12:YES textfieldd:sender];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self setViewMovedUp12:NO textfieldd:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([string isEqualToString:@"."]) {
        if ([priceTxt.text containsString:@"."]) {
            
            NSString *text;
            text =[[textField text] stringByAppendingString:string];
            NSString *newString = [text substringToIndex:[text length]-1];
            
            priceTxt.text=newString;
            return NO;
        }
    }
    
    return YES;
}




#pragma mark -
#pragma mark Text View Delegate


- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    [self.keyboardControls setActiveField:textView];
    if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
        [self setViewMovedUp:YES textfieldd:textView];
    } else if ([[UIScreen mainScreen]bounds].size.height==667) {
        [self setViewMovedUp:YES textfieldd:textView];
    }
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    [self setViewMovedUp:NO textfieldd:textView];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    [descriptionView resignFirstResponder];
    [priceTxt resignFirstResponder];
}

#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction {
    UIView *view;
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        view = field.superview.superview;
    } else {
        view = field.superview.superview.superview;
    }
    
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

-(void)setViewMovedUp:(BOOL)movedUp textfieldd:(UITextView *)textView {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    CGRect rect = self.view.frame;
    if (movedUp) {
        if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
            rect.origin.y=-80;
        } else if ([[UIScreen mainScreen]bounds].size.height==667) {
            rect.origin.y=-40;
        }
    }
    else {
        rect.origin.y=0;
    }
    
    self.view.frame = CGRectMake(0, rect.origin.y, rect.size.width, rect.size.height);
    
    [UIView commitAnimations];
    
}


-(void)setViewMovedUp12:(BOOL)movedUp textfieldd:(UITextField *)textfield {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    CGRect rect = self.view.frame;
    if (movedUp) {
        if ([[UIScreen mainScreen]bounds].size.height==568 || [[UIScreen mainScreen]bounds].size.height==480) {
            rect.origin.y=-105;
        } else if ([[UIScreen mainScreen]bounds].size.height==667) {
            rect.origin.y=-70;
        } else  {
            rect.origin.y=-50;
        }
    }
    else {
        rect.origin.y=0;
    }
    
    self.view.frame = CGRectMake(0, rect.origin.y, rect.size.width, rect.size.height);
    
    [UIView commitAnimations];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
