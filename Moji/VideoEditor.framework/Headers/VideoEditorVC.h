
//static NSString *const VEFilter;
static NSString *const VEAdjust;
static NSString *const VECrop;
static NSString *const VEMusic;
static NSString *const VETrim;
static NSString *const VEText;
static NSString *const VEEnhance;
static NSString *const VEOrientation;
static NSString *const VEBorder;
static NSString *const VESticker;
static NSString *const VEFrames;
static NSString *const VESpeed;

#define VEFilter @"VEFilter"

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@protocol VideoEditorSDKDelegate <NSObject>
@optional
-(void)videoEditorSDKFinishedWithUrl:(NSURL *)url;
-(void)videoEditorSDKCanceled;
@end

@interface VideoEditorVC : UIViewController
@property (nonatomic, weak) id<VideoEditorSDKDelegate> delegate;
@property (nonatomic,strong) NSURL *videoPath;
@property (nonatomic,strong) NSString *clientKey;
@property (nonatomic,strong) NSString *clientSecretKey;
@property (nonatomic,strong) NSArray *excludedFunctionalities;
@end
