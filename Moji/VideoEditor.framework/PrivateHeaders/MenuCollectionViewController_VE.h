
#import <UIKit/UIKit.h>
#import "MenuCollectionViewCell_VE.h"

@protocol MenuDelegate <NSObject>
@optional
-(void)getSelectedMenu:(int)menuIndex;
@end

@interface MenuCollectionViewController : UICollectionViewController
@property (nonatomic, weak) id<MenuDelegate> delegate;
@property (strong, nonatomic) IBOutlet UICollectionView *menuCollectionView;
-(void)func_refreshCategories;
@end
